#!/bin/bash

if [ ! -d resultados ]; then
	mkdir resultados
fi

for file in "$@"
do
	
	instance="$(basename "$file")"
	instance="${instance%.*}"
	resultsfile="resultados/$instance"_P3IG_results.txt
	
	if [ -e ./Debug/qapmetheuristics.exe ]; then
		./Debug/qapmetheuristics.exe $file > "$resultsfile"
	fi
	if [ -e ./Debug/qapmetheuristics ]; then
		./Debug/qapmetheuristics $file > "$resultsfile"
	fi
	if [ -e ./Debug/QAPMetheuristics ]; then
		./Debug/QAPMetheuristics $file > "$resultsfile"
	fi

	output="resultados/P3IG_$instance".png

cat << _end_ | gnuplot
	set terminal png
	set output "$output"
	set title "$instance - Iterated Greedy"
	set key right top
	set xlabel "Numero de ejecuciones"
	set ylabel "Fitness"
	set logscale y
	plot '$resultsfile' using 7 with lines title 'Current IG', '$resultsfile' using 8 with lines title 'Best IG'
_end_
	
	echo -e "Generado: $output"



done

