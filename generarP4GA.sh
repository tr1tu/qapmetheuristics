#!/bin/bash

if [ ! -d resultados ]; then
	mkdir resultados
fi

for file in "$@"
do
	
	instance="$(basename "$file")"
	instance="${instance%.*}"
	resultsfile="resultados/$instance"_P4GA_results.txt
	
	if [ -e ./Debug/qapmetheuristics.exe ]; then
		./Debug/qapmetheuristics.exe $file > "$resultsfile"
	fi
	if [ -e ./Debug/qapmetheuristics ]; then
		./Debug/qapmetheuristics $file > "$resultsfile"
	fi
	if [ -e ./Debug/QAPMetheuristics ]; then
		./Debug/QAPMetheuristics $file > "$resultsfile"
	fi

	output="resultados/P4GA_$instance".png

cat << _end_ | gnuplot
	set terminal png
	set output "$output"
	set title "$instance - Genetic Algorithm"
	set key right top
	set xlabel "Numero de ejecuciones"
	set ylabel "Fitness"
	plot '$resultsfile' using 1 with points ps 0.2 title 'Current GA', '$resultsfile' using 2 with lines title 'Best GA'
_end_
	
	echo -e "Generado: $output"



done

