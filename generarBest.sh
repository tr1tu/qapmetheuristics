#!/bin/bash

if [ ! -d resultados ]; then
	mkdir resultados
fi

for file in "$@"
do
	
	instance="$(basename "$file")"
	instance="${instance%.*}"
	resultsfile="resultados/$instance"_results.txt
	./Debug/QAPMetheuristics $file > "$resultsfile"

	output="resultados/$instance".png

cat << _end_ | gnuplot
	set terminal png
	set output "$output"
	set title "$instance"
	set key right top
	set xlabel "Numero de ejecuciones"
	set ylabel "Fitness"
	
	plot '$resultsfile' using 1 with points ps 0.2 title 'Current GA', '$resultsfile' using 2 with lines title 'Best GA', '$resultsfile' using 3 with points ps 0.2 title 'Current ACO', '$resultsfile' using 4 with lines title 'Best ACO'
_end_
	
	echo -e "Generado: $output"



done

#set logscale x