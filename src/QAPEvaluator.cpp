/*
 * QAPEvaluator.cpp
 *
 *  Created on: 20 feb. 2017
 */

#include "QAPEvaluator.h"
#include "QAPInstance.h"
#include "QAPSolution.h"

unsigned QAPEvaluator::_numEvaluations = 0;

double QAPEvaluator::computeFitness(QAPInstance &instance, QAPSolution &solution)
{
	/*
	 * Calcular el fitness de la solución.
	 */

	double fitness;

	fitness = instance.getSumDistanceTimesFlow(solution);


	_numEvaluations++;
	return fitness;

}

double QAPEvaluator::computeDeltaFitnessSwap(QAPInstance& instance,
		QAPSolution& solution, int indexFacility1, int indexFacility2) {

	_numEvaluations++;

	//Como el fitness es getSumDistanceTimesFlow, el deltaFitness es la
	//variación de este valor.
	double deltaFitness = instance.getDeltaSumDistanceTimesFlowSwap(solution, indexFacility1, indexFacility2);

	return deltaFitness;


}

double QAPEvaluator::computeDeltaFitnessAssignment(QAPInstance& instance,
				QAPSolution& solution, int indexFacility, int indexLocation)
{
	_numEvaluations++;

	double deltaFitness = instance.getDeltaSumDistanceTimesFlowAssignment(solution, indexFacility, indexLocation);

	return deltaFitness;

}

void QAPEvaluator::resetNumEvaluations() {
	_numEvaluations = 0;
}

