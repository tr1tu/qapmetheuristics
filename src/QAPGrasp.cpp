/*
 * QAPGrasp.cpp
 *
 * Fichero que define las funciones de la clase QAPGrasp. Forma parte del código esqueleto para el problema de las múltiples mochilas cuadráticas, ofrecido para las prácticas de la asignatura Metaheurísticas del Grado de Ingeniería Informática de la Universidad de Córdoba
 *
 * @author Carlos García cgarcia@uco.es
 */

#include "QAPGrasp.h"
#include <vector>
#include "QAPSimpleFirstImprovementNO.h"
#include "QAPLocalSearch.h"
#include "QAPSolGenerator.h"
#include <iostream>

using namespace std;

void QAPGrasp::chooseOperation(QAPFacilitySwapOperation& operation) {

	int bestFacility1 = 0;
	int bestFacility2 = 0;
	double bestDeltaFitness = 0;
	bool initialisedBestFitness = false;
	unsigned numFacilities = _instance->getSize();

	/**
	 * Calcular el número de intentos como el porcentaje _alpha por el número de posibilidades, que es el número de departamentos por el número de localizaciones.
	 */

	unsigned numTries = ((unsigned)(numFacilities * numFacilities * _alpha));

	/**
	 * Generar alternativas de intercambio de dos departamentos aleatorias.
	 */

	for (unsigned i = 0; i < numTries; i++) {
		int indexFacility1 = rand() % numFacilities;
		int indexFacility2 = rand() % numFacilities;

		double deltaFitness = QAPEvaluator::computeDeltaFitnessSwap(*_instance, *_sol, indexFacility1, indexFacility2); // obtener la mejora en fitness de dicha operación

		//actualizar si resulta ser la mejor
		if (deltaFitness < bestDeltaFitness || initialisedBestFitness == false) {
			initialisedBestFitness = true;
			bestFacility1 = indexFacility1;
			bestFacility2 = indexFacility2;
			bestDeltaFitness = deltaFitness;
		}
	}

	operation.setValues(bestFacility1, bestFacility2, bestDeltaFitness);
}

void QAPGrasp::buildInitialSolution() {

	/**
	 * Vaciar la solución _sol poniendo cada departamento con una localización y calculando su fitness.
	 */
	/*unsigned numFacilities = _instance->getSize();

	for (unsigned i = 0; i < numFacilities; i++) {
		_sol->assignFacilityToLocation(i, i);
	}*/

	//Generamos una solución inicial aleatoria.
	QAPSolGenerator::genRandomSol(*_instance, *_sol);

	_sol->setFitness(QAPEvaluator::computeFitness(*_instance, *_sol));

	/** Seleccionar la primera operación */
	QAPFacilitySwapOperation operation;
	chooseOperation(operation);

	/**
	 * Mientras la operación tenga un incremento de fitness positivo, operation.getDeltaFitness(),
	 *  1. aplicar la operación en _sol
	 *  2. Almacenar el fitness de la solución en _result (para las gráficas)
	 *  3. seleccionar una nueva operación
	 */
	while (operation.getDeltaFitness() < 0) {
		operation.apply(*_sol);
		_results.push_back(_sol->getFitness());
		chooseOperation(operation);
	}
}

void QAPGrasp::initialise(double alpha, QAPInstance& instance) {

	_sol = new QAPSolution(instance);
	_bestSolution = new QAPSolution(instance);
	_bestSolution->copy(*_sol);
	_instance = &instance;
	_alpha = alpha;
}

void QAPGrasp::run(QAPStopCondition& stopCondition) {

	if (_sol == NULL) {
		cerr << "GRASP was not initialised" << endl;
		exit(-1);
	}

	/**
	 * Mientras no se alcance el criterio de parada
	 *   1. Generar una solución inicial invocando al método correspondiente
	 *   2. Almacenar el fitness de la solución en _results
	 *   3. Optimizar _sol con la búsqueda local y el operador de vecindario de la metaheurística
	 *   4. Actualizar la mejor solución
	 */
	while (!stopCondition.reached()) {
			//std::cout << "GRASP" << std::endl;
			buildInitialSolution();
			_results.push_back(_sol->getFitness());
			_ls.optimise(*_instance, _no, *_sol);

			vector<double> &auxResults = _ls.getResults();

			for (auto result : auxResults){
				_results.push_back(result);
			}

			if (QAPEvaluator::compare(_sol->getFitness(), _bestSolution->getFitness()) > 0)
				_bestSolution->copy(*_sol);

			stopCondition.notifyIteration();
		}
}
