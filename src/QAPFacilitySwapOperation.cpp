/*
 * QAPFacilitySwapOperation.cpp
 *
 *  Created on: 9 mar. 2017
 */

#include "QAPFacilitySwapOperation.h"

QAPFacilitySwapOperation::QAPFacilitySwapOperation()
{
	_indexFacility1 = 0;
	_indexFacility2 = 0;
	_deltaFitness = 0;
}

QAPFacilitySwapOperation::~QAPFacilitySwapOperation(){ }

void QAPFacilitySwapOperation::apply(QAPSolution &solution)
{
	/*
	 * Aplicamos la oepración de intercambio de dos departamentos
	 * en la solución recibida.
	 * 1. Intercambiamos la localización de los dos departamentos.
	 * 2. Actualizamos el fitness de la solución.
	 */

	//Obtenemos las localizaciones de ambos departamentos.
	int location1 = solution.whereIsFacility(_indexFacility1);
	int location2 = solution.whereIsFacility(_indexFacility2);

	//Intercambiamos las localizaciones.
	solution.assignFacilityToLocation(_indexFacility1, location2);
	solution.assignFacilityToLocation(_indexFacility2, location1);

	//Actualizamos el fitness de la solución sumándole deltaFiness.
	solution.setFitness(solution.getFitness() + _deltaFitness);
}

void QAPFacilitySwapOperation::setValues(int indexFacility1, int indexFacility2, double deltaFitness)
{
	//Asignamos los valores de los atributos de clase.
	_indexFacility1 = indexFacility1;
	_indexFacility2 = indexFacility2;
	_deltaFitness = deltaFitness;
}
