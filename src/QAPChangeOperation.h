/*
 * QAPChangeOperation.h
 *
 *  Created on: 9 mar. 2017
 */

#ifndef SRC_QAPCHANGEOPERATION_H_
#define SRC_QAPCHANGEOPERATION_H_

#include "QAPSolution.h"

class QAPChangeOperation
{
public:

	/**
	 * Destructor
	 */
	virtual ~QAPChangeOperation(){
	}

	/**
	 * Función que aplica el cambio que define el objeto sobre la solución que recibe como argumento
	 * @param[in,out] solution Objeto solución sobre el que se aplicará el cambio
	 */
	virtual void apply(QAPSolution &solution) = 0;

};

#endif /* SRC_QAPCHANGEOPERATION_H_ */
