/*
 * QAPSolGenerator.cpp
 *
 *  Created on: 20 feb. 2017
 */

#include "QAPSolGenerator.h"
#include "QAPInstance.h"
#include "QAPSolution.h"

#include <list>
#include <cstdlib>

void QAPSolGenerator::genRandomSol(QAPInstance &instance, QAPSolution &solution)
{
	// Generar solucion aleatoria. Asignar cada departamento a un lugar aleatorio no ocupado.
	/*std::list<int> _facility;
	std::list<int>::iterator _it;*/
	std::vector<int> perm;
	instance.randomPermutation(instance.getSize(), perm);

	for(int i = 0; i < instance.getSize(); i++)
		solution.assignFacilityToLocation(i, perm[i]);

	/*for(int i=0; i<instance.getSize(); i++)
	{
		_facility.push_back(i);
	}
	for(unsigned int i=0; i<_facility.size(); i++)
	{
		_it = _facility.begin();
		for(unsigned int j = 0; j< (rand() % _facility.size()); j++)
		{
			_it++;
		}
		solution.assignFacilityToLocation(i, *_it);
		_facility.erase(_it);
	}*/

}
