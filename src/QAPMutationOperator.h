/*
 * QAPMutationOperator.h
 *
 */

#ifndef INCLUDE_QAPMUTATIONOPERATOR_H_
#define INCLUDE_QAPMUTATIONOPERATOR_H_

#include "QAPSolution.h"
#include <vector>
#include <cstdlib>

using namespace std;

/**
 * Clase que define un operador de mutación para el QAP. Se basa en recorrer todos los genes de la solución y cambiarlos por un valor aleatorio según una probabilidad
 */
class QAPMutationOperator{

protected:
	/**
	 * Variables miembro de la clase
	 * _mutProb Probabilidad de mutación
	 * _size Tamaño de la instancia.
	 */
	double _mutProb;
	unsigned _size;

	/**
	 * Función que muta una solución
	 * @param[in,out] sol Solución a mutar
	 */
	void mutate(Solution* sol){
		QAPSolution *s = (QAPSolution*) sol;

		//Recorrer los departamentos y, según la probabilidad de mutación,
		//intercambiarlo con otro departamento aleatorio.
		for(unsigned i = 0; i < _size; i++)
		{
			double r = ((double)rand()) / RAND_MAX;

			if(r < _mutProb)
			{
				int randFacility = rand() % _size;
				int location1 = s->whereIsFacility(i);
				s->assignFacilityToLocation(i, s->whereIsFacility(randFacility));
				s->assignFacilityToLocation(randFacility, location1);
			}
		}
	}

public:
	/**
	 * Constructor
	 * @param[in] mutProb Probabilidad de mutación
	 * @param[in] instance Instancia del problema a abordar
	 */
	QAPMutationOperator(double mutProb, QAPInstance &instance){
		_mutProb = mutProb;
		_size = instance.getSize();
	}

	/**
	 * Función que muta un conjunto de soluciones
	 * @param[in,out] sols Soluciones a mutar
	 */
	void mutate(vector<Solution*> &sols){

		for (Solution* sol : sols){
			mutate(sol);
		}
	}
};



#endif /* INCLUDE_QAPMUTATIONOPERATOR_H_ */
