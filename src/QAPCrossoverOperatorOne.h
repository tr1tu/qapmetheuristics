/*
 * QAPCrossoverOperatorOne.h
 *
 *  Created on: 13 may. 2017
 *      Author: victor
 */

#ifndef SRC_QAPCROSSOVEROPERATORONE_H_
#define SRC_QAPCROSSOVEROPERATORONE_H_


#include "QAPSolution.h"
#include "Solution.h"
#include "QAPInstance.h"
#include <vector>
#include <algorithm> //min, max
#include <iostream>
#include "QAPCrossoverOperator.h"

using namespace std;

/**
 * Clase que implementa un operador de cruce uniforme para el problema QAP.
 */
class QAPCrossoverOperatorOne : public QAPCrossoverOperator {

protected:
	/**
	 * Variables miembro de la clase:
	 *  - _instance Instancia de problema abordada. Se utiliza únicamente para crear nuevos objetos QAPSolution
	 *  - _numObjs almacena el tamaño de la instancia
	 *  - _crossProb probabilidad de cruce
	 */
	QAPInstance *_instance;
	unsigned _size;
	double _crossProb;

	/**
	 * Función que cruza dos soluciones según su probabilidad de cruce. En caso de que no haya crucce, la solución devuelta será igual al primer padre
	 * @param[in] s1 primer padre
	 * @param[in] s2 segundo padre
	 * @return Nuevo objeto solución descendiente de haber cruzado s1 y s2. La solución se reserva dinámicamente en memoria. Es responsabilidad del invocador de gestionarla correctamente.
	 */
	QAPSolution * cross(Solution *s1, Solution *s2) {
		QAPSolution * sol = new QAPSolution(*_instance);
		QAPSolution * sol1 = (QAPSolution *) s1;
		QAPSolution * sol2 = (QAPSolution *) s2;

		//Una probabilidad == 1 haría que el algoritmo convergiese demasiado rápido
		double randSample = (((double) rand()) / RAND_MAX);

		if (randSample < _crossProb) {
			/*
			 * El cruce consistirá en seleccionar un conjunto de departamentos consecutivos del padre 1 e
			 * insertarlos en la nueva solución. Estos elementos se marcarán en el padre 2 para que no se
			 * vuelvan a seleccionar. Los departamentos restantes se tomarán del padre 2 y se empezará a
			 * rellenar por la derecha.
			 */

			int a = rand() % _size;
			int b = rand() % _size;
			int index1 = min(a,b);
			int index2 = max(a,b);
			QAPSolution * auxSol2 = new QAPSolution(*_instance);
			auxSol2->copy(*sol2);

			for(int i = index1; i <= index2; i++)
			{
				int iL = sol1->whereIsFacility(i);
				sol->assignFacilityToLocation(i, iL);
				//Marcamos con el valor de location -10 los departamentos en el padre 2.
				int iF = auxSol2->whichFacilityIsThere(iL);
				auxSol2->assignFacilityToLocation(iF, -10);
			}

			int indexSol2 = _size - 1;
			int indexSol = _size - 1;

			while(indexSol >= 0)
			{
				if(indexSol >= index1 && indexSol <= index2) {
					indexSol--;
					continue;
				}

				if(auxSol2->whereIsFacility(indexSol2) == -10) {
					indexSol2--;
					continue;
				}

				sol->assignFacilityToLocation(indexSol, auxSol2->whereIsFacility(indexSol2));
				indexSol--;
				indexSol2--;
			}

			delete auxSol2;
			auxSol2 = NULL;


		} else {
			//Si no hay cruce, copiar el primer padre
			sol->copy(*sol1);
		}


		return sol;
	}

public:

	/**
	 * Constructor
	 * @param[in] crossProb Probabilidad de cruce
	 * @param[in] instance Instancia del problema abordada
	 */
	QAPCrossoverOperatorOne(double crossProb, QAPInstance &instance) {
		_instance = &instance;
		_size = instance.getSize();
		_crossProb = crossProb;
	}

	/**
	 * Función que aplica el cruce a una población de padres.
	 * @param[in] parents Vector de padres. El cruce se aplica entre cada dos padres consecutivos (1,2), (3,4)...
	 * @param[out] offspring Vector donde se almacenan los descendientes generados. IMPORTANTE: Esta función reserva memoria dinámicamente para las nuevas soluciones en offspring, por lo que es responsabilidad de quien la invoque de gestionar la memoria adecuadamente.
	 */
	virtual void cross(vector<Solution*> &parents, vector<Solution*> &offspring) {

		unsigned numParents = (unsigned) parents.size();

		//aplicar cruce entre cada dos padres consecutivos (1,2), (3,4), ...
		for(unsigned i=0; i<numParents-1; i+=2) {
			QAPSolution *sol = cross(parents[i], parents[i+1]);
			offspring.push_back(sol);
			sol = cross(parents[i], parents[i+1]);
			offspring.push_back(sol);
		}
	}
};


#endif /* SRC_QAPCROSSOVEROPERATORONE_H_ */
