/*
 * QAPCrossoverOperator.h
 *
 *  Created on: 13 may. 2017
 *      Author: victor
 */

#ifndef SRC_QAPCROSSOVEROPERATOR_H_
#define SRC_QAPCROSSOVEROPERATOR_H_

#include "QAPSolution.h"
#include "Solution.h"
#include <vector>

class QAPCrossoverOperator
{
public:
	virtual ~QAPCrossoverOperator(){};
	virtual void cross(vector<Solution*> &parents, vector<Solution*> &offspring) = 0;
};


#endif /* SRC_QAPCROSSOVEROPERATOR_H_ */
