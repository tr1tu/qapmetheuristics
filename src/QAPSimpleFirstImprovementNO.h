/*
 * QAPSimpleFirstImprovementNO.h
 *
 *  Created on: 9 mar. 2017
 *      Author: victor
 */

#ifndef SRC_QAPSIMPLEFIRSTIMPROVEMENTNO_H_
#define SRC_QAPSIMPLEFIRSTIMPROVEMENTNO_H_

#include "QAPNeighExplorer.h"
#include "QAPSolution.h"
#include "QAPChangeOperation.h"
#include "QAPInstance.h"
#include "QAPEvaluator.h"
#include <stdlib.h>
#include <iostream>
#include <vector>
#include "QAPFacilitySwapOperation.h"

using namespace std;

class QAPSimpleFirstImprovementNO : public QAPNeighExplorer {

	public:

		virtual ~QAPSimpleFirstImprovementNO(){

		}

		virtual bool findOperation(QAPInstance &instance, QAPSolution &solution, QAPChangeOperation &operation);

};

#endif /* SRC_QAPSIMPLEFIRSTIMPROVEMENTNO_H_ */
