/*
 * QAPAntColonyOpt.h
 *
 */

#ifndef INCLUDE_QAPANTCOLONYOPT_H_
#define INCLUDE_QAPANTCOLONYOPT_H_

#include "QAPMetaheuristic.h"
#include "QAPInstance.h"
#include "QAPSolution.h"
#include "QAPFacilitySwapOperation.h"
#include "QAPFacilityAssignmentOperation.h"
#include <vector>
#include <unordered_set>
#include <iostream>
#include <cmath>
#include <limits>

using namespace std;

/**
 * Clase que implementa un sistema de colonias de  hormigas para el QAP.
 */

class QAPAntColonyOpt: public QAPMetaheuristic {

	/**
	 * Clase que define implementa internamente a una hormiga para el QAP
	 */
	class QAPAnt {
	protected:

		/**
		 * Variables miembro de la clase
		 * Básicas:
		 *  _colony puntero a la clase que la engloba (su colonia)
		 *  _sol solución sobre la que trabaja la hormiga
		 *
		 * Adicionales
		 *  _objectsLeft Conjunto de departamentos que no tienen una localización asignada. Esta variable permite acelerar el proceso de construcción de soluciones de la hormiga. Su objetivo es mayormente hacer el proceso más eficiente
		 *  _candidateListSize Número de opciones a considerar para añadir una nueva componente a la solución. Esta variable permite acelerar el proceso de construcción de soluciones, al impedir que se examinen todas las opciones posibles.
		 */
		QAPAntColonyOpt *_colony;
		QAPSolution *_sol;
		unordered_set<unsigned> _facilitiesLeft;
		double _candidateListSize;

		/**
		 * Función que suma los valores de un vector. Se usará para conocer la suma de los valores de relevancia de las opciones que tiene la hormiga
		 * @param[in] significances vector con las relevancias de las opciones de la hormiga
		 * @return suma de los valores del vector de relevancias
		 */
		double sumSignificances(vector<double> &significances) {

			double sum = 0;

			for(unsigned i = 0; i< significances.size(); i++){
				sum += significances[i];
			}

			return sum;
		}

		/**
		 * Función que devuelve un conjunto de alternativas que tiene una hormiga para añadir una nueva componente a su solución.
		 * @param[out] alternatives Vector donde se almacenan las alternativas que tiene la hormiga
		 * @param[out] significances Vector con los valores de relevancia de las alternativas creadas
		 */
		void createAlternatives(
				vector<QAPFacilitySwapOperation*> &alternatives,
				vector<double> &significances) {

			//Obtener la información de la colonia
			QAPInstance *instance = _colony->_instance;
			double alpha = _colony->_alpha;
			double beta = _colony->_beta;
			vector<vector<double>*> &phMatrix = _colony->_phMatrix;

			unsigned numTries = 0;

			//Para cada departamento que no esté asignado aún.
			for (unsigned indexFacility : _facilitiesLeft) {

				//Para cada posible departamento y sin superar el número de intentos.
				//for (int indexFacility2 = 0; indexFacility2 < _colony->_instance->getSize(); indexFacility2++)
				for(unsigned indexFacility2 : _facilitiesLeft)
				{
					if(numTries >= _candidateListSize)
						break;

					//Obtener el deltaFitness y contarlo como un intento
					double deltaFitness = QAPEvaluator::computeDeltaFitnessSwap(*instance,
							*_sol, (int)indexFacility, (int)indexFacility2);
					numTries++;

					//Saltarse las opciones a peor o nulas (no debieran ocurrir si no hay profits negativos, "pero pa por si")
					if (deltaFitness >= 0)
						continue;

					/**
					 *
					 * 1. Crear la operación
					 * 2. Calcular su relevancia como la densidad del objeto^beta * cantidadPheromona[objeto][mochila]^alpha
					 * 3. Incluir la operación en las alternatives y la relevancia en significances
					 */
					QAPFacilitySwapOperation *al =
							new QAPFacilitySwapOperation();
					double density = -deltaFitness;
					double relevance = pow(density,beta)
							* pow((phMatrix.at(indexFacility))->at(_sol->whereIsFacility(indexFacility2)),alpha)
							* pow((phMatrix.at(indexFacility2))->at(_sol->whereIsFacility(indexFacility)),alpha);

					al->setValues((int)indexFacility,(int)indexFacility2,deltaFitness);
					alternatives.push_back(al);
					significances.push_back(relevance);
				}
			}
		}

		/**
		 * Función que devuelve la mejor alternativa de la hormiga para añadir una nueva componente a su solución, de entre un conjunto de alternativas aleatorias evaluadas.
		 * @param[out] op alternativa seleccionada como la mejor para la hormiga
		 */
		void selectBestAlternative(QAPFacilitySwapOperation &op) {

			//Obtener la información de la colonia
			QAPInstance *instance = _colony->_instance;
			vector<vector<double>*> &phMatrix = _colony->_phMatrix;
			double beta = _colony->_beta;
			double alpha = _colony->_alpha;

			double bestSignificance = -1;
			unsigned numTries = 0;

			//Para cada departamento que aún no tenga localización asignada.
			for (unsigned indexFacility : _facilitiesLeft) {

				//Para cada posible departamento y sin superar el número de intentos
				//for (int indexFacility2 = 0; indexFacility2 < _colony->_instance->getSize(); indexFacility2++)
				for(unsigned indexFacility2 : _facilitiesLeft)
				{
					if(numTries >= _candidateListSize)
						break;

					//Obtener el deltaFitness y contarlo como un intento
					double deltaFitness = QAPEvaluator::computeDeltaFitnessSwap(*instance,
							*_sol, (int)indexFacility, (int)indexFacility2);
					numTries++;

					//Saltarse las opciones a peor o nulas (no debieran ocurrir si no hay profits negativos, "pero pa por si")
						if (deltaFitness >= 0)
							continue;
					/**
					 *
					 * 1. Calcular su relevancia como la densidad del objeto^beta * cantidadPheromona[objeto][mochila]^alpha
					 * 2. Si es mejor que la mejor hasta ahora, guardarla en op
					 */

						double density = -deltaFitness;
						double relevance = pow(density,beta)
										* pow((phMatrix.at(indexFacility))->at(_sol->whereIsFacility(indexFacility2)),alpha)
										* pow((phMatrix.at(indexFacility2))->at(_sol->whereIsFacility(indexFacility)),alpha);
						if(bestSignificance < relevance){
							bestSignificance = relevance;
							op.setValues(indexFacility,indexFacility2,deltaFitness);
						}
				}
			}
		}

		/**
		 * Función que libera la memoria de las alternativas creadas por la hormiga para elegir una
		 * @param[in,out] alt Vector con las alternativas a liberar de memoria
		 */
		void freeAlternatives(vector<QAPFacilitySwapOperation*> &alt) {

			for (auto altOp : alt) {
				delete altOp;
			}

			alt.clear();
		}

	public:

		/**
		 * Constructor de una hormiga
		 * @param[in] candidateListSize Número de soluciones a evaluar en cada paso
		 * @param[in] colony Puntero a la colonia a la que pertenece la hormiga
		 */
		QAPAnt(unsigned candidateListSize, QAPAntColonyOpt *colony) {
			_colony = colony;
			_sol = new QAPSolution(*(colony->_instance));
			_candidateListSize = candidateListSize;
		}

		/**
		 * Destructor
		 */
		~QAPAnt() {
			delete _sol;
			_sol = NULL;
		}

		/**
		 * Función que resetea la memoria de la hormiga para que empieze a generar una solución desde cero
		 */
		void resetSolution() {

			/**
			 *
			 * 1. Asignar todos los departamentos a la localización -1 e insertarlos en la memoria _facilitiesLeft.
			 * 2. Asignarles un fitness infinito.
			 */
			//No es necesario limpiar la memoria _objectsLeft ya que se un conjunto
			//(no puede tener elementos repetidos).

			vector<int> perm;
			_colony->_instance->randomPermutation(_colony->_instance->getSize(), perm);

			for(int i = 0; i < _colony->_instance->getSize(); i++)
			{
				_sol->assignFacilityToLocation(i, perm[i]);
				_facilitiesLeft.emplace(i);
				_sol->setFitness(QAPEvaluator::computeFitness(*(_colony->_instance), *_sol));
			}
		}

		/**
		 * Función que hace que la hormiga escoja una alternativa y la añada a su solución. También devuelve la opción escogida
		 * @param[out] operation Operación de asignación de un departamento a una localización elegida por la hormiga
		 */
		void chooseOperation(QAPFacilitySwapOperation &operation) {

			//Decidir entre elegir la mejor altnerativa o una según probabilidades
			double randSample = (((double) rand()) / RAND_MAX);

			if (randSample < _colony->_q0) {
				selectBestAlternative(operation);
			} else {

				//Crear las alternativas
				vector<QAPFacilitySwapOperation*> alternatives;
				vector<double> significances;
				createAlternatives(alternatives, significances);

				//Si la hormiga no encontró alternativas, salir
				if (significances.size() <= 0) {
					return;
				}

				//Elegir una de las alternativas según probabilidades proporcionales a sus relevancias
				double v_sumSignificances = sumSignificances(significances);
				double randSample = (((double) rand()) / RAND_MAX)
						* v_sumSignificances;
				randSample -= significances.at(0);
				unsigned opSelected = 0;

				while (randSample > 0 && opSelected < alternatives.size() - 1) {
					opSelected++;
					randSample -= significances.at(opSelected);
				}

				//Asignar la alternativa elegida en operation
				unsigned indexFacility = alternatives.at(opSelected)->getFacility1();
				unsigned indexFacility2 =
						alternatives.at(opSelected)->getFacility2();
				double deltaFitness =
						alternatives.at(opSelected)->getDeltaFitness();
				operation.setValues(indexFacility, indexFacility2, deltaFitness);

				//Liberar las alterantivas de memoria
				freeAlternatives(alternatives);
			}

			//Si se seleccionó alguna alternativa, aplicarla a la solución y eliminar el objeto correspondiente de _objectsLeft
			if (operation.getFacility1() >= 0 && operation.getFacility2() >= 0) {
				operation.apply(*_sol);
				_facilitiesLeft.erase(operation.getFacility1());
				_facilitiesLeft.erase(operation.getFacility2());
			}
		}

		/**
		 * Función que devuelve la solución construída por la hormiga
		 * @return Solución construída por la hormiga
		 */
		QAPSolution & getSolution() {
			return *_sol;
		}
	};

protected:

	/**
	 * Variables miembro de la colonia de hormigas:
	 *  _q0 Probabilidad de que cada hormiga elija la mejor alternativa en vez de una en base a probabilidades
	 *  _alpha Relevancia de la cantidad de feromona al calcular la relevancia de cada alternativa
	 *  _beta Relevancia de la información heurística al calcular la relevancia de cada alternativa
	 *  _initTau Cantidad de feromona inicial en el entorno (no interesa que sea 0)
	 *  _evaporation Porcentaje de feromona que se evapora
	 *  _phMatrix Matriz de feromona 2D. El primer índice recorre los objetos del problema. El segundo recorre las mochilas.
	 *  _ants vector hormigas de la colonia
	 *  _instance Instancia del problema abordado
	 */
	double _q0;
	double _alpha;
	double _beta;
	double _initTau;
	double _evaporation;
	vector<vector<double>*> _phMatrix;
	vector<QAPAnt*> _ants;
	QAPInstance *_instance;

	/**
	 * vectores donde se almacenan los resultados
	 *  _results valores fitness de las soluciones generadas
	 *  _bestPerIteration Mejor fitness generado en cada iteración
	 *  _currentItMeans Media de los valores fitness de las soluciones generadas en cada iteración
	 */
	vector<double> _results;
	vector<double> _bestPerIteration;
	vector<double> _currentItMeans;

	/**
	 * Función que aplica la actualización local de feromona (cuando una hormiga anda, se lleva parte de pheromona; ver fórmula en diapositivas)
	 * @param[in] op Opción que escogió la hormiga y donde se va a aplicar la actualización
	 */
	/*void localUpdate(QAPFacilitySwapOperation &op) {

		int fac1 = op.getFacility1();
		int fac2 = op.getFacility2();

		//_phMatrix.at(fac1)->at(fac2) = (1 - _evaporation) * _phMatrix.at(fac1)->at(fac2) + _evaporation * _initTau;
		_phMatrix.at(fac1)->at(fac2) = (1 - _evaporation) * _phMatrix.at(fac1)->at(fac2) + _evaporation * _initTau;
	}*/

	void localUpdate(QAPFacilityAssignmentOperation &op) {

			int fac = op.getFacility();
			int loc = op.getLocation();

			//_phMatrix.at(fac1)->at(fac2) = (1 - _evaporation) * _phMatrix.at(fac1)->at(fac2) + _evaporation * _initTau;
			_phMatrix.at(fac)->at(loc) = (1 - _evaporation) * _phMatrix.at(fac)->at(loc) + _evaporation * _initTau;
		}

	/**
	 * Función que libera a las hormigas para que construyan sus soluciones
	 */
	void releaseAnts() {

		unordered_set<unsigned> movingAnts;
		unordered_set<unsigned> stoppedAnts;
		int i = 0;

		//Resetear las soluciones de cada hormiga e insertar sus índices en movingAnts
		for (auto ant : _ants) {
			ant->resetSolution();
			movingAnts.emplace(i);
			i++;
		}

		//Mientras haya hormigas que se estén moviendo
		while (movingAnts.size() > 0) {
			stoppedAnts.clear();

			//Mover cada hormiga
			for (auto iAnt : movingAnts) {
				QAPAnt *ant = _ants[iAnt];
				QAPFacilitySwapOperation op;
				op.setValues(-1, -1, 0);
				ant->chooseOperation(op);

				//Si la hormiga se ha movido, entonces aplicar la actualización local de feromona.
				//Si no, apuntarla en stoppedAnts para eliminarla después de movingAnts
				//Si se ha movido, el objeto será mayor o igual que 0.
				if ( op.getFacility1() >= 0) {
					QAPFacilityAssignmentOperation aOp1, aOp2;
					aOp1.setValues(
								op.getFacility1(),
								ant->getSolution().whereIsFacility((int)op.getFacility2()),
								op.getDeltaFitness()
							);
					aOp2.setValues(
								op.getFacility2(),
								ant->getSolution().whereIsFacility((int)op.getFacility1()),
								op.getDeltaFitness()
							);
					//localUpdate(op);
					localUpdate(aOp1);
					localUpdate(aOp2);

				} else {
					stoppedAnts.emplace(iAnt);
				}
			}

			for (auto iAnt : stoppedAnts) {
				movingAnts.erase(iAnt);
			}
		}

		//Actualizar la mejor Solución
		double bestFitness = _bestSolution->getFitness();

		for (auto ant : _ants) {
			QAPSolution &sol = ant->getSolution();
			double currentFitness = ant->getSolution().getFitness();

			if (QAPEvaluator::compare(currentFitness, bestFitness) > 0) {
				_bestSolution->copy(sol);
				bestFitness = currentFitness;
			}
		}
	}

	/**
	 * Función que guarda estadísticas de las soluciones generadas en sus vectores miembro
	 */
	void saveStatistics() {

		QAPSolution &firstSol = _ants.at(0)->getSolution();
		double bestFitness = firstSol.getFitness();
		double meanFitness = 0.;
		unsigned numAnts = (unsigned) _ants.size();
		double inverseNumAnts = 1. / numAnts;

		for (auto ant : _ants) {
			QAPSolution &sol = ant->getSolution();
			double currentFitness = sol.getFitness();
			_results.push_back(currentFitness);
			meanFitness += (currentFitness * inverseNumAnts);

			if (QAPEvaluator::compare(currentFitness, bestFitness) > 0) {
				bestFitness = currentFitness;
			}
		}

		_bestPerIteration.push_back(bestFitness);
		_currentItMeans.push_back(meanFitness);
	}

	/**
	 * Función que ejecuta una iteración del algoritmo ACO, es decir, liberar las hormigas para que construyan sus soluciones, y actualizar la matriz de pheromona
	 */
	void iterate() {

		//Liberar las hormigas
		releaseAnts();
		saveStatistics();

		//aplicar pheromona con la mejor solución
		unsigned size = _instance->getSize();
		double fitness = _bestSolution->getFitness();

		//Para cada objeto, depositar feromona en el par objeto y mochila en la que está dicho objeto.
		//Depositamos una cantidad igual al fitness.
		for (unsigned i = 0; i < size; i++) {
			/*_phMatrix.at(i)->at(_bestSolution->whereIsFacility(i)) = (1 - _evaporation)
								* _phMatrix.at(i)->at(_bestSolution->whereIsFacility(i)) + _evaporation * (1./fitness);*/
			_phMatrix.at(i)->at(_bestSolution->whereIsFacility(i)) = (1 - _evaporation)
								* _phMatrix.at(i)->at(_bestSolution->whereIsFacility(i)) + _evaporation * (1./fitness);
		}

	}

public:
	/**
	 * Constructor
	 */
	QAPAntColonyOpt() {
		_bestSolution = NULL;
		_q0 = 0.8;
		_alpha = 1;
		_beta = 1;
		_initTau = 0.1;
		_evaporation = 0.1;
		_instance = NULL;
	}

	/**
	 * Destructor
	 */
	~QAPAntColonyOpt() {

		if (_bestSolution != NULL) {
			delete _bestSolution;
			_bestSolution = NULL;
		}

		for (auto ant : _ants) {
			delete ant;
		}
		_ants.clear();

		for (auto vector : _phMatrix) {
			vector->clear();
			delete vector;
		}

		_phMatrix.clear();
	}

	/**
	 * Función que inicializa el algoritmo
	 * @param[in] numAnts Número de hormigas en la colonia
	 * @param[in] q0 Probabilidad de que las hormigas elijan la mejor opción posible, en vez de basada en probabilidades
	 * @param[in] alpha Relevancia de la cantidad de feromona depositada al evaluar las alternativas
	 * @param[in] beta Relevancia de la heurística al evaluar las alternativas
	 * @param[in] initTau Cantidad inicial de feromona en el entorno
	 * @param[in] evaporation Ratio de evaporación de feromona
	 * @param[in] candidateListSize Número de alternativas que cada hormiga evalúa a la hora de elegir una opción a añadir a la solución
	 * @param[in] instance Instancia del problema que se va a abordar
	 */
	void initialise(unsigned numAnts, double q0, double alpha, double beta,
			double initTau, double evaporation, unsigned candidateListSize,
			QAPInstance &instance) {
		_instance = &instance;
		_q0 = q0;
		_alpha = alpha;
		_beta = beta;
		_initTau = initTau;
		_evaporation = evaporation;

		if (numAnts <= 0) {
			cerr << "The number of ants must be greater than 0" << endl;
			exit(1);
		}

		if (_bestSolution != NULL) {
			delete _bestSolution;
			_bestSolution = NULL;
		}


		//Generación de una solución inicial para _bestSolution
		_bestSolution = new QAPSolution(*_instance);
		QAPSolGenerator::genRandomSol(*_instance, *_bestSolution);
		double fitness = QAPEvaluator::computeFitness(*_instance,
				*_bestSolution);
		_bestSolution->setFitness(fitness);

		//Creación de las hormigas
		for (unsigned i = 0; i < numAnts; i++) {
			QAPAnt *ant = new QAPAnt(candidateListSize, this);
			_ants.push_back(ant);
		}


		//Inicialización de la matriz de feromona con la feromona inicial
		unsigned size = _instance->getSize();

		for (unsigned i = 0; i < size; i++) {
			vector<double> *aVector = new vector<double>;
			_phMatrix.push_back(aVector);

			for (unsigned j = 0; j < size; j++) {
				aVector->push_back(_initTau);
			}
		}
	}

	/**
	 * Función que ejecuta el algoritmo ACO
	 * @param[in] stopCondition Objeto que define cuándo se llega a la condición de parada
	 */
	virtual void run(QAPStopCondition &stopCondition) {

		if (_instance == NULL) {
			cerr << "The ACO algorithm has not been initialised" << endl;
			exit(1);
		}

		//Mientras no se llegue a la condición de parada, iterar
		while (stopCondition.reached() == false) {
			iterate();
			stopCondition.notifyIteration();
		}
	}

	/**
	 * Función que devuelve el vector con las mejores soluciones generadas por iteración
	 * @return Las mejores soluciones generadas en cada iteración
	 */
	const vector<double>& getBestPerIteration() const {
		return _bestPerIteration;
	}

	/**
	 * Función que devuelve el vector con la media de las soluciones generadas en cada iteración
	 * @return La media de las soluciones generadas en cada iteración
	 */
	const vector<double>& getAntsMeanResults() const {
		return _currentItMeans;
	}

	/**
	 * Función que devuelve el fitness de las soluciones generadas en cada momento
	 * @return El fitness de las soluciones generadas en cada momento
	 */
	vector<double>& getResults() {
		return _results;
	}
};

#endif /* INCLUDE_QAPANTCOLONYOPT_H_ */
