/*
 * QAPSimpleBestImprovementNO.cpp
 *
 *  Created on: 9 mar. 2017
 *      Author: victor
 */

#include "QAPSimpleBestImprovementNO.h"
#include "QAPNeighExplorer.h"
#include "QAPInstance.h"
#include "QAPSolution.h"
#include "QAPChangeOperation.h"
#include "QAPEvaluator.h"
#include <iostream>

bool QAPSimpleBestImprovementNO::findOperation(QAPInstance &instance, QAPSolution &solution, QAPChangeOperation &operation)
{
	QAPFacilitySwapOperation *oaOperation = dynamic_cast<QAPFacilitySwapOperation*>(&operation);
	if (oaOperation == NULL){
		cerr << "MQKPSimpleFirstImprovementNO::findOperation recibió un objeto operation que no es de la clase MQKPObjectAssignmentOperation" << endl;
		exit(1);
	}

	int numFacilities = instance.getSize();
	double bestDeltaFitness = 0;
	double deltaFitness;
	bool initialized = false;

	//std::cout << "Fitness de la solucion: " << solution.getFitness() << std::endl;

	for(int i=0; i<numFacilities-1; i++)
	{
		for(int j=i+1; j<numFacilities; j++)
		{
			deltaFitness = QAPEvaluator::computeDeltaFitnessSwap(instance, solution, i, j);
			//std::cout << "DeltaFitness >>" << deltaFitness << "<<";

			if(deltaFitness < bestDeltaFitness || initialized == false)
			{
				initialized = true;

				bestDeltaFitness = deltaFitness;
				oaOperation->setValues(i, j, deltaFitness);
			}
			//std::cout << "BestDeltaFitness >>" << bestDeltaFitness << "<<" << std::endl;
		}
	}

	return bestDeltaFitness < 0;
}

