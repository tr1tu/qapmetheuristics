/*
 * TournamentSelector.h
 */

#ifndef INCLUDE_TOURNAMENTSELECTOR_H_
#define INCLUDE_TOURNAMENTSELECTOR_H_

#include "Solution.h"
#include "SelectionOperator.h"
#include <vector>

using namespace std;

/**
 * Clase que implementa el operador de selección por torneo
 */
class TournamentSelector : public SelectionOperator {

protected:
	/**
	 * Variables miembro de la clase
	 * _k número de participantes en el torneo
	 */
	unsigned _k;

	/**
	 * Función que selecciona una solución del conjunto mediante torneo
	 * @param[in] set Vector de soluciones
	 * @result Solución seleccionada
	 */
	Solution* selectOne (vector<Solution*> &set){

		/**
		 * 1. Seleccionar una solución aleatoria como la actualmente ganadora
		 *
		 * 2. Repetir (_k-1) veces la selección de otra solución aleatoria y el torneo con la actualmente ganadora (mantener la mejor)
		 */
		Solution * best;
		Solution * newSolution;

		best = set[rand() % set.size()];

		for(unsigned i = 0; i < _k; i++){
			newSolution = set[rand() % set.size()];
			if(newSolution->hasValidFitness()){
				if(!best->hasValidFitness()){
					best=newSolution;
				}
				else if(QAPEvaluator::compare(newSolution->getFitness(), best->getFitness() > 0)){
					best = newSolution;
				}
			}
		}

		return best;
	}

public:

	/**
	 * Constructor
	 * @param[in] k Número de participantes en el torneo
	 */
	TournamentSelector(unsigned k){
		_k = k;
	}

	/**
	 * Destructor
	 */
	virtual ~TournamentSelector(){}

	/**
	 * Función que selecciona tantas parejas de padres de un vector como elementos hay en dicho vector
	 * @param[in] orig Vector de soluciones sobre el que aplicar la selección
	 * @param[out] result Vector donde se almacenan las parejas de padres seleccionadas
	 */
	virtual void select(vector<Solution*> &orig, vector<Solution*> &result){

		//Utilizando le método propio selectOne, seleccionar tantas parejas
		//de padres como elementos hay en orig
		for(unsigned i = 0; i < orig.size(); i++)
		{
			result.push_back(selectOne(orig));
			result.push_back(selectOne(orig));
		}
	}
};



#endif /* INCLUDE_TOURNAMENTSELECTOR_H_ */
