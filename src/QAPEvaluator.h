/*
 * QAPEvaluator.h
 *
 *  Created on: 20 feb. 2017
 */

#ifndef __QAPEVALUATOR_H__
#define __QAPEVALUATOR_H__

#include "QAPInstance.h"
#include "QAPSolution.h"



class QAPEvaluator {
protected:
	/**
	 * Variable donde se contabiliza el número de soluciones que se evalúan a través de  computeFitness o computeDeltaFitness
	 */
	static unsigned _numEvaluations;
public:

	/*
	 * Función que calcula el fitness de una solución.
	 */
	static double computeFitness(QAPInstance &instance, QAPSolution &solution);

	static double computeDeltaFitnessSwap(QAPInstance& instance,
			QAPSolution& solution, int indexFacility1, int indexFacility2);

	static double computeDeltaFitnessAssignment(QAPInstance& instance,
				QAPSolution& solution, int indexFacility, int indexLocation);

	/**
	 * Función que resetea la variable interna que contabiliza el número de evaluaciones
	 */
	static void resetNumEvaluations();

	/**
	 * Función que devuelve el número de veces que se ha evaluado alguna solución
	 */
	static unsigned getNumEvaluations() {
		return _numEvaluations;
	}

	/**
		 * Función que realiza la comparación entre dos valores de fitness (sirve para especificar si es un problema de maximización o minimización)
		 * @param[in] f1 primer valor de fitness a comparar
		 * @param[in] f2 segundo valor de fitness a comparar
		 * @return Un valor positivo si el primer valor es mejor que el segundo, negativo en caso contrario, y 0 si son indistinguibles
		 */
		static double compare(double f1, double f2){
			return (f2 - f1);
		}

		/**
		 * Función que indica si el problema es de minimización o de maximización
		 * @return Devuelve true si el problema es de minimización o false, si es de maximización
		 */
		static bool isToBeMinimised(){
			return (compare(0,1) > 0);
		}
};

#endif /* __QAPEVALUATOR_H__ */
