/*
 * QAPLocalSearch.h
 *
 *  Created on: 9 mar. 2017
 */

#ifndef SRC_QAPLOCALSEARCH_H_
#define SRC_QAPLOCALSEARCH_H_

#include "QAPInstance.h"
#include "QAPSolution.h"
#include "QAPNeighExplorer.h"

class QAPLocalSearch {

	/**
	 * vector de doubles donde almacena la calidad de la última solución aceptada
	 */
	std::vector<double> _results;

public:
	/**
	 * Constructor
	 */
	QAPLocalSearch();

	/**
	 * Destructor
	 */
	~QAPLocalSearch();

	/**
	 * Función que optimiza una solución aplicado repetidamente la exploración de su vecindario hasta alcanzar un óptimo local.
	 * @param[in] instance Instancia del problema
	 * @param[in] explorer Operador de exploración del vecindario. La idea es que reciba un operador que bien explore el vecindario devolviendo la primera solución que mejora a la actual, o devolviendo el mejor cambio posible sobre la solución actual
	 * @param[in,out] solution Solución inicial que se optimiza localmente. El resultado final se devuelve en ella.
	 */
	void optimise(QAPInstance &instance, QAPNeighExplorer &explorer, QAPSolution &solution);

	/**
	 * Función que devuelve el vector con los resultados de las soluciones aceptadas, en cada paso, por la búsqueda local
	 *
	 * @return vector con los resultados de las soluciones aceptadas, en cada paso, por la búsqueda local
	 */
	std::vector<double>& getResults() {
		return _results;
	}


};

#endif /* SRC_QAPLOCALSEARCH_H_ */
