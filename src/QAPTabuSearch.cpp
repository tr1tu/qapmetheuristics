/*
 * QAPTabuSearch.cpp
 *
 * Fichero que define las funciones de la clase QAPTabuSearch. Forma parte del código esqueleto para el problema de las múltiples mochilas cuadráticas, ofrecido para las prácticas de la asignatura Metaheurísticas del Grado de Ingeniería Informática de la Universidad de Córdoba
 *
 * @author Carlos García cgarcia@uco.es
 */

#include "QAPEvaluator.h"
#include "QAPTabuSearch.h"
#include <initializer_list>
#include <iostream>
#include <utility>
#include <vector>
#include <cstdlib>
#include "QAPFacilitySwapOperation.h"

using namespace std;

void QAPTabuSearch::initialise(QAPInstance* instance, unsigned tabuTennure) {
	_instance = instance;
	_tabuTennure = tabuTennure;
}

void QAPTabuSearch::setSolution(QAPSolution* solution) {

	if (_solution != NULL){
		cerr << "No se debe invocar más de una vez el método QAPTabuSearch::setSolution" << endl;
		exit(1);
	}

	this->_solution = solution;

	if (_bestSolution == NULL) {
		_bestSolution = new QAPSolution(*_instance);
	}

	_bestSolution->copy(*solution);
}

void QAPTabuSearch::run(QAPStopCondition& stopCondition) {
	if (_solution == NULL) {
		cerr << "Tabu search has not been given an initial solution" << endl;
		exit(-1);
	}

	_results.clear();
	unsigned numFacilities = _instance->getSize();
	unsigned numIterations = 0;

	/**
	 *
	 * Mientras no se alcance la condición de parada
	 *  1. Generar una permutación de objetos
	 *  2. Buscar la mejor operación no tabú de asignación de un objeto a una mochila (incluida la 0)
	 *  3. Aplicar la operación
	 *  4. Insertar el índice del objeto afectado en la memoria tabú
	 *  5. Actualizar la mejor solución hasta el momento
	 */

	while (!stopCondition.reached()) {

		vector<int> perm;
		QAPInstance::randomPermutation(numFacilities, perm);
		double bestDeltaFitness = 0;
		bool initialisedDeltaFitness = false;
		QAPFacilitySwapOperation bestOperation;
		std::pair<unsigned, unsigned> is_tabu;
		//Buscar la mejor operación no tabú
		for (unsigned i = 0; i < numFacilities; i++) {
			unsigned indexFacility = perm[i];

			//Si el departamento no es tabú (utilizar _shortTermMem_aux.find)
			//if (!_shortTermMem_aux.find(indexFacility)) {

			//Probar todos los departamentos y elegir la mejor opción
			for (unsigned j = 0; j<numFacilities; j++) {
				//Saltarse el cambio que no hace nada
				if ( indexFacility == j)
					continue;
				is_tabu = make_pair(indexFacility, _solution->whereIsFacility(j));

				if(_shortTermMem_aux.find(is_tabu) == _shortTermMem_aux.end())
				{
					//Obtener la diferencia de fitness de aplicar dicha operación
					double deltaFitness = QAPEvaluator::computeDeltaFitnessSwap(*_instance,*_solution,(int)indexFacility,(int)j);

					//Si la diferencia de fitness es la mejor hasta el momento, apuntarla para aplicarla después
					//deltaFitness < bestDeltafitness ya que es de minimización
					if (deltaFitness < bestDeltaFitness
							|| initialisedDeltaFitness == false) {
						initialisedDeltaFitness = true;
						bestDeltaFitness = deltaFitness;
						bestOperation.setValues(indexFacility,j,deltaFitness);

					}
				}
			}
		}


		//Aplicar la operación y almacenarla en la memoria a corto plazo
		bestOperation.apply(*_solution);
		_shortTermMem.push(bestOperation.getFac(*_solution));
		_shortTermMem.push(bestOperation.getInvFac(*_solution));

		_shortTermMem_aux.insert(bestOperation.getFac(*_solution));
		_shortTermMem_aux.insert(bestOperation.getInvFac(*_solution));

		//Si hay demasiados elementos en la memoria, según la tenencia tabú, eliminar los dos más antiguos.
		//Eliminamos dos ya que se añaden de dos en dos.
		if (_shortTermMem.size() > _tabuTennure) {
			std::pair<unsigned,unsigned> value = _shortTermMem.front();
			_shortTermMem.pop();
			_shortTermMem_aux.erase(value);

			value = _shortTermMem.front();
			_shortTermMem.pop();
			_shortTermMem_aux.erase(value);
		}

		//Actualizar la mejor solución
		if (QAPEvaluator::compare(_solution->getFitness(),
				_bestSolution->getFitness()) > 0) {
			_bestSolution->copy(*_solution);
		}

		numIterations++;
		_results.push_back(_solution->getFitness());

		stopCondition.notifyIteration();

	}
}
