/*	main.cpp
 *
 */

#define P2

#ifdef P1
//Definición de algunos parámetros de la experimentación
#define MAX_SECONS_PER_RUN 10
#define MAX_SOLUTIONS_PER_RUN 100000
#define NUM_RUNS 1


#include "QAPEvaluator.h"
#include "QAPInstance.h"
#include "QAPSolGenerator.h"
#include "QAPSolution.h"
#include "Timer.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <vector>
#include <algorithm>
#include <cfloat>

using namespace std;


//Definición externa de las semillas para el generador de números aleatorios (en seeds.cpp)
extern unsigned int numSeeds;
extern unsigned int seeds[];

/**
 * Función que realiza la generación de soluciones aleatorias, durante MAX_SECONS_PER_RUN segundos y un máximo de MAX_SOLUTIONS_PER_RUN, para la instancia proporcionada
 * @param[out] results vector donde se almacenarán los valores fitness de las soluciones aleatorias que se generen
 * @param[in] insstance Instancia del problema de la mochila cuadrática múltiple
 */
void runARandomSearchExperiment(vector<double> &results, QAPInstance &instance){

	QAPSolution currentSolution(instance);
	Timer t;
	unsigned int numSolutions = 0;

	//Mientras no se acabe el tiempo y no se generen demasiadas soluciones, generar solución aleatoria y guardar el resultado
	while (t.elapsed_time(Timer::VIRTUAL) <= MAX_SECONS_PER_RUN && numSolutions < MAX_SOLUTIONS_PER_RUN){
		QAPSolGenerator::genRandomSol(instance, currentSolution);
		double currentFitness = QAPEvaluator::computeFitness(instance, currentSolution);
		results.push_back(currentFitness);
		numSolutions++;
	}

}

/**
 * Función que ejecuta todos los experimentos para los argumentos pasados al programa principal, en particular NUM_RUNS experimentos para cada instancia
 * @param[out] results matriz 3D donde se almacenarán los resultados. El primer índice será para la instancia considerada. El segundo para el experimento sobre dicha instancia. El tercero para la solución generada en dicho experimento
 * @param[in] mainArgs Argumentos de la función main (argv). En los argumentos vienen, desde el índice 1, <nombre del fichero de la instancia 1> <número de mochilas> <nombre del fichero de la instancia 2> <número de mochilas>...
 * @param[in] numMainArgs Número de argumentos de la función main (argc)
 */
void runExperiments(vector< vector< vector< double>* >* > &results, char **mainArgs, unsigned int numMainArgs){

	//Para cada instancia del problema
	for (unsigned int iInstance = 1; iInstance <= numMainArgs; iInstance++){
		//Leer la instancia y crear la estructuras de datos necesarias para guardar los resultados
		QAPInstance instance;
		vector< vector < double >* >* resultsOnThisInstance = new vector< vector< double >* >;
		results.push_back(resultsOnThisInstance);
		char *instanceName = mainArgs[iInstance];
		instance.readInstance(instanceName);

		//Lanzar NUM_RUNS ejecuciones sobre dicha instancia
		for (unsigned int r = 1; r <= NUM_RUNS && r < numSeeds; r++){
			srand(seeds[r]); //Inicialización del generador de números aleatorios al inicio de cada ejecución
			vector<double> *theseResults = new vector<double>;
			resultsOnThisInstance->push_back(theseResults);
			runARandomSearchExperiment(*theseResults, instance);
		}
	}
}

/**
 * Función que devuelve el número de soluciones en la ejecución con más soluciones
 * @param[in] results Matriz 2D con los resultados de las distintas ejecuciones. El primer índice es para las ejecuciones. El segundo es para las soluciones de una misma ejecución
 * @return Número de soluciones en la ejecución con más soluciones.
 */
unsigned int getLongestRunLength(vector< vector< double >* > &results){

	unsigned int maxLength = 0;
	unsigned int numRuns = results.size();

	for (unsigned int iRun = 0; iRun < numRuns; iRun++){

		unsigned int length = results.at(iRun)->size();
		if (length > maxLength){
			maxLength = length;
		}
	}

	return maxLength;
}

/**
 * Estructura para calcular y almacenar los datos de salida del programa, media de las soluciones de la iteración actual y media de las mejores soluciones desde el inicio hasta la iteración actual
 */
struct currentBestPair{
	double current;
	double best;
};

/**
 * Función que calcula las medidas de los resultados de varias ejecuciones.
 * @param[in] results Matriz 2D con los resultados de las distintas ejecuciones. El primer índice es para las ejecuciones. El segundo es para las soluciones de una misma ejecución
 * @param[out] means Vector donde se almacenan los resultados medios en datos de tipo currentBestPair, es decir, con la media de las soluciones generadas en la iteración actual y la media de las mejores soluciones generadas desde el inicio hasta la iteración actual
 * @return Devuelve el número de elementos en el vector de salida means
 */
unsigned int computeMeans(vector< vector<double>* > &results, vector<currentBestPair> &means){
	//Obtener el número de soluciones en la ejecución más larga
	unsigned int longestRunLength = getLongestRunLength(results);
	unsigned int numRuns = results.size();
	currentBestPair previousMean;
	previousMean.best = -DBL_MAX;

	//Calcular la media
	for (unsigned int iSolutions = 0; iSolutions < longestRunLength; iSolutions++){
		currentBestPair currentMean;
		currentMean.current = 0;
		currentMean.best = 0;
		double value;

		if (iSolutions == 0){

			for (unsigned int iRun = 0; iRun < numRuns; iRun++){

				unsigned int length = results.at(iRun)->size();
				if (length <= iSolutions){
					value = results.at(iRun)->back();
					currentMean.current += value;
				}else{
					value = results.at(iRun)->at(iSolutions);
					currentMean.current += value;
				}

				currentMean.best += value;
			}
		}
		else {
			for (unsigned int iRun = 0; iRun < numRuns; iRun++){

				unsigned int length = results.at(iRun)->size();
				if (length <= iSolutions){
					value = results.at(iRun)->back();
					currentMean.current += value;
				}else{
					value = results.at(iRun)->at(iSolutions);
					currentMean.current += value;
				}

				currentMean.best += min(value, previousMean.best);
			}
		}

		currentMean.current /= numRuns;
		currentMean.best /= numRuns;
		means.push_back(currentMean);
		previousMean.best = currentMean.best;
	}

	return longestRunLength;
}

/**
 * Función que libera la memoria de un vector 2D
 * @param[in] array vector 2D que se va a liberar de memoria
 */
template <typename T>
void free2Darray(vector< vector<T>* > &array){

	int numVectors = array.size();

	for (int i = 0; i < numVectors; i++){
		array.at(i)->clear();
		delete (array.at(i));
	}

	array.clear();
}

/**
 * Función que libera la memoria de un vector 3D
 * @param[in] array matriz 3D que se va a liberar de memoria
 */
template <typename T>
void free3Darray(vector< vector <vector<T>*>*> &array){

	int numArrays = array.size();

	for (int i = 0; i < numArrays; i++){
		free2Darray(*(array.at(i)));
		delete (array.at(i));
	}

	array.clear();
}

/**
 * Función main que ejecuta la experimentación. Generación de soluciones aleatorias para las instancias
 *
 * Finalmente se escribe en la salida estandar los resultados generados en forma de matriz. Por cada instancia se generan dos columnas, una con los resultados
 * medios de las soluciones generadas en cada iteración y otra con los resultados medios de las mejores soluciones generadas desde el inicio hasta dicha iteración
 */
int main(int argc, char **argv) {

	if (argc < 2){
		cout << argv[0] << " (<problem instance files>)+" << endl;
		return 0;
	}

	//////////////////////////
	//Ejecución de experimentos
	//////////////////////////
	unsigned int numInstances = (argc - 1);
	vector< vector< vector< double >* >* > allTheResults;
	//En la matriz allTheResults se almacenarán todos los valores de fitness generados
	//Es tridimensional
	//El primer índice recorre las instancias de los problemas que se han abordado
	//El segundo índice recorre las ejecuciones que se han hecho sobre la misma instancia
	//El tercer índice recorre las soluciones que se han generado en la misma ejecución

	runExperiments(allTheResults, argv, argc);

	////////////////////////////
	//Calculo de los valores medios
	////////////////////////////
	unsigned int overallMaxNumResults = 0;
	vector< vector< currentBestPair>* > meanResults;
	//En la matriz meanResults se almacenarán los resultados pero haciendo la media en cada iteración de la búsqueda (Media de las primeras soluciones de las diferentes ejecuciones, igual de las segundas soluciones, etc.)
	//Es bidimensional
	//El primer índice recorre las instancias de los problemas que se han abordado
	//El segundo índice recorre los valores medios calculados por cada iteración del método

	//Para cada instancia del problema, calcular sus medias
	for (unsigned int iInstance = 0; iInstance < numInstances; iInstance++){

		vector< vector< double >* > *resultsThisInstance = allTheResults.at(iInstance);
		vector< currentBestPair > *theseMeans = new vector<currentBestPair>;
		meanResults.push_back(theseMeans);
		unsigned int numResults = computeMeans(*resultsThisInstance, *theseMeans);

		if (numResults > overallMaxNumResults){
			overallMaxNumResults = numResults;
		}
	}



	//////////////////////
	// Impresión de resultados
	//////////////////////
	for (int iInstance = 1; iInstance < argc; iInstance++){
		cout << argv[iInstance] << "_best\t";
		cout << argv[iInstance] << "_current\t";
	}
	cout << endl;

	for (unsigned int iIteration = 0; iIteration < overallMaxNumResults; iIteration++){
		for (unsigned int iInstance = 0; iInstance < numInstances; iInstance++){
			cout << meanResults.at(iInstance)->at(iIteration).best << "\t";
			cout << meanResults.at(iInstance)->at(iIteration).current << "\t";
		}
		cout << endl;
	}

	//////////////////////
	// Liberar memoria
	//////////////////////
	free3Darray(allTheResults);
	free2Darray(meanResults);

	return 0;
}

#endif

#ifdef P2
//Definición de algunos parámetros de la experimentación
#define MAX_SECONS_PER_RUN 30
#define MAX_SOLUTIONS_PER_RUN 500000
#define MAX_INITIAL_SOLUTIONS 5

#include "QAPEvaluator.h"
#include "QAPInstance.h"
#include "QAPSolGenerator.h"
#include "QAPSolution.h"
#include "QAPLocalSearch.h"
#include "QAPSimpleFirstImprovementNO.h"
#include "QAPSimpleBestImprovementNO.h"
#include "Timer.h"
#include <iostream>

using namespace std;

//Definición externa de las semillas para el generador de números aleatorios (en seeds.cpp)
//extern unsigned int numSeeds;
extern unsigned int seeds[];

/**
 * Función que realiza varias optimizaciones locales de soluciones aleatorias, durante MAX_SECONS_PER_RUN segundos, un máximo de MAX_SOLUTIONS_PER_RUN y un máximo de MAX_INITIAL_SOLUTIONS optimizaciones locales, para la instancia proporcionada
 * @param[out] currentResults vector donde se almacenarán los valores fitness de las soluciones que la búsqueda local va aceptando en to_do momento.
 * @param[out] bestSoFarResults vector donde se almacenarán los mejores valores fitness generados hasta el momento
 * @param[in] instance Instancia del problema de la mochila cuadrática múltiple
 * @param[in] explorer Operador de exploración de vecindarios a utilizar
 */
void runALSExperiment(vector<double> &currentResults,
		vector<double> &bestSoFarResults, QAPInstance &instance,
		QAPNeighExplorer &explorer) {

	//Inicialización
	QAPLocalSearch ls;
	QAPSolution initialSolution(instance);
	Timer t;
	QAPEvaluator::resetNumEvaluations();

	//Generar una primera solución aleatoria para inicializar bestFitness
	QAPSolGenerator::genRandomSol(instance, initialSolution);
	double currentFitness = QAPEvaluator::computeFitness(instance,
			initialSolution);
	double bestFitness = currentFitness;
	initialSolution.setFitness(currentFitness);
	currentResults.push_back(currentFitness);
	bestSoFarResults.push_back(bestFitness);
	int numInitialSolutions = 0;

	//Mientras no se acabe el tiempo y no se generen demasiadas soluciones,	generar solución aleatoria, aplicarle la optimización y guardar el resultado
	while (t.elapsed_time(Timer::VIRTUAL) <= MAX_SECONS_PER_RUN
			&& QAPEvaluator::getNumEvaluations() < MAX_SOLUTIONS_PER_RUN &&
			numInitialSolutions < MAX_INITIAL_SOLUTIONS) {

		/*
		 * Generar una nueva solución aleatoria en initialSolution
		 * e inicializa su fitness
		 */
		QAPSolGenerator::genRandomSol(instance, initialSolution);
		initialSolution.setFitness(QAPEvaluator::computeFitness(instance, initialSolution));

		currentResults.push_back(currentFitness);
		bestSoFarResults.push_back(
				min(bestSoFarResults.back(), currentFitness));

		// Invoca a QAPLocalSearch::optimise para optimizar la solución

		ls.optimise(instance, explorer,initialSolution);

		//Almacenar los resultados
		vector<double> &resultsLS = ls.getResults();

		for (auto aResult : resultsLS) {
			currentResults.push_back(aResult);
			bestSoFarResults.push_back(min(bestSoFarResults.back(), aResult));
		}

		numInitialSolutions++;
	}

	cout << "Time:" << t.elapsed_time(Timer::VIRTUAL) << endl;

}

/**
 * Función que ejecuta todos los experimentos para los argumentos pasados al programa principal
 * @param[out] results matriz 3D donde se almacenarán los resultados. El primer índice será para la instancia considerada. El segundo para el experimento sobre dicha instancia. El tercero para la solución generada en dicho experimento
 * @param[in] mainArgs Argumentos de la función main (argv). En los argumentos vienen, desde el índice 1, <nombre del fichero de la instancia 1> <número de mochilas> <nombre del fichero de la instancia 2> <número de mochilas>...
 * @param[in] numMainArgs Número de argumentos de la función main (argc)
 */
void runExperiments(vector<vector<vector<double>*>*> &results, char **mainArgs,
		unsigned int numMainArgs) {

	QAPSimpleFirstImprovementNO firstExplorer;
	QAPSimpleBestImprovementNO bestExplorer;

	//Para cada instancia del problema
	for (unsigned int iInstance = 1; iInstance < numMainArgs; iInstance++) {

		//Leer la instancia y crear la estructuras de datos necesarias para guardar los resultados
		QAPInstance instance;
		vector<vector<double>*>* resultsOnThisInstance = new vector<
				vector<double>*>;
		results.push_back(resultsOnThisInstance);
		char *instanceName = mainArgs[iInstance];
		instance.readInstance(instanceName);

		//Ejecutar la búsqueda local con primera mejora
		vector<double> *theseFirstResults = new vector<double>;
		vector<double> *bestFirstResults = new vector<double>;
		resultsOnThisInstance->push_back(theseFirstResults);
		resultsOnThisInstance->push_back(bestFirstResults);
		runALSExperiment(*theseFirstResults, *bestFirstResults, instance,
				firstExplorer);


		//Ejecutar la búsqueda local con mejor mejora
		vector<double> *theseBestResults = new vector<double>;
		vector<double> *bestBestResults = new vector<double>;
		resultsOnThisInstance->push_back(theseBestResults);
		resultsOnThisInstance->push_back(bestBestResults);
		runALSExperiment(*theseBestResults, *bestBestResults, instance,
				bestExplorer);
	}
}

/**
 * Función que libera la memoria de un vector 2D
 * @param[in] array vector 2D que se va a liberar de memoria
 */
template<typename T>
void free2Darray(vector<vector<T>*> &array) {

	int numVectors = array.size();

	for (int i = 0; i < numVectors; i++) {
		array.at(i)->clear();
		delete (array.at(i));
	}

	array.clear();
}

/**
 * Función que libera la memoria de un vector 3D
 * @param[in] array matriz 3D que se va a liberar de memoria
 */
template<typename T>
void free3Darray(vector<vector<vector<T>*>*> &array) {

	int numArrays = array.size();

	for (int i = 0; i < numArrays; i++) {
		free2Darray(*(array.at(i)));
		delete (array.at(i));
	}

	array.clear();
}

/**
 * Función main que ejecuta la experimentación. Optimización local de soluciones aleatorias para las instancias
 *
 * Finalmente se escribe en la salida estandar los resultados generados en forma de matriz. Por cada instancia se generan cuatro columnas, una con los resultados
 * que va aceptando la búsqueda local de primera mejora en cada momento, otra con los mejores resultados hasta el momento de la búsqueda local anterior,
 * otra con los resultados que va aceptando la búsqueda local de mejor mejora en cada momento y una última con los mejores resultados hasta el momento de la búsqueda local anterior
 */
int main(int argc, char **argv) {

	if (argc < 2) {
		cout << argv[0] << " (<problem instance files>)+"
				<< endl;
		return 0;
	}

	//////////////////////////
	//Ejecución de experimentos
	//////////////////////////
	unsigned int numInstances = argc - 1;
	vector<vector<vector<double>*>*> allTheResults;
	srand(seeds[0]);
	//En la matriz allTheResults se almacenarán todos los valores de fitness generados
	//Es tridimensional
	//El primer índice recorre las instancias de los problemas que se han abordado
	//El segundo índice recorre las ejecuciones que se han hecho sobre la misma instancia
	//El tercer índice recorre las soluciones que se han generado en la misma ejecución

	runExperiments(allTheResults, argv, argc);






	//////////////////////
	// Impresión de resultados
	//////////////////////
	vector<double> lastResults;
	for (unsigned int iInstance = 0; iInstance < numInstances; iInstance++){
		cout << argv[iInstance+1] << "_currentFirst\t";
		cout << argv[iInstance+1] << "_bestFirst\t";
		cout << argv[iInstance+1] << "_currentBest\t";
		cout << argv[iInstance+1] << "_bestBest\t";
		lastResults.push_back(allTheResults.at(iInstance)->at(0)->at(0));
		lastResults.push_back(allTheResults.at(iInstance)->at(1)->at(0));
		lastResults.push_back(allTheResults.at(iInstance)->at(2)->at(0));
		lastResults.push_back(allTheResults.at(iInstance)->at(3)->at(0));
	}
	cout << endl;
	bool allResultsPrinted = false;
	unsigned int iIteration = 0;
	unsigned int indexColumn = 0;

	while (allResultsPrinted == false){
		allResultsPrinted = true;
		for (unsigned int iInstance = 0; iInstance < numInstances; iInstance++){

			if (allTheResults.at(iInstance)->at(0)->size() > iIteration){
				allResultsPrinted = false;
				cout << allTheResults.at(iInstance)->at(0)->at(iIteration) << "\t";
				lastResults[indexColumn] = allTheResults.at(iInstance)->at(0)->at(iIteration);
			} else {
				cout << lastResults[indexColumn] << "\t";
			}
			indexColumn++;

			if (allTheResults.at(iInstance)->at(0)->size() > iIteration){
				allResultsPrinted = false;
				cout << allTheResults.at(iInstance)->at(1)->at(iIteration) << "\t";
				lastResults[indexColumn] = allTheResults.at(iInstance)->at(1)->at(iIteration);
			} else {
				cout << lastResults[indexColumn] << "\t";
			}
			indexColumn++;

			if (allTheResults.at(iInstance)->at(2)->size() > iIteration){
				allResultsPrinted = false;
				cout << allTheResults.at(iInstance)->at(2)->at(iIteration) << "\t";
				lastResults[indexColumn] = allTheResults.at(iInstance)->at(2)->at(iIteration);
			} else {
				cout << lastResults[indexColumn] << "\t";
			}
			indexColumn++;

			if (allTheResults.at(iInstance)->at(3)->size() > iIteration){
				allResultsPrinted = false;
				cout << allTheResults.at(iInstance)->at(3)->at(iIteration) << "\t";
				lastResults[indexColumn] = allTheResults.at(iInstance)->at(3)->at(iIteration);
			} else {
				cout << lastResults[indexColumn] << "\t";
			}
			indexColumn++;
		}
		cout << endl;
		iIteration++;
		indexColumn = 0;
	}




	//////////////////////
	// Liberar memoria
	//////////////////////
	free3Darray(allTheResults);
	return 0;
}

#endif

#ifdef P3
//Definición de algunos parámetros de la experimentación
#define MAX_SECONS_PER_RUN 60
#define MAX_SOLUTIONS_PER_RUN 50000
#define MAX_ITERATIONS 0

#include "QAPEvaluator.h"
#include "QAPInstance.h"
#include "QAPSolGenerator.h"
#include "QAPSolution.h"
#include "QAPSimulatedAnnealing.h"
#include "QAPTabuSearch.h"
#include "QAPGrasp.h"
#include "QAPIteratedGreedy.h"
#include "QAPStopCondition.h"
#include "Timer.h"

#include <iostream>
using namespace std;

//Definición externa de las semillas para el generador de números aleatorios (en seeds.cpp)
//extern unsigned int numSeeds;
extern unsigned int seeds[];

/**
 * Función que ejecuta el enfriamiento simulado, durante MAX_SECONS_PER_RUN segundos, un máximo de MAX_SOLUTIONS_PER_RUN, para la instancia proporcionada
 * @param[out] currentResults vector donde se almacenarán los valores fitness de las soluciones que va aceptando el enfriamiento simulado.
 * @param[out] bestSoFarResults vector donde se almacenarán los mejores valores fitness generados hasta el momento
 * @param[in] instance Instancia del problema de la mochila cuadrática múltiple
 */
void runASAExperiment(vector<double> &currentResults,
		vector<double> &bestSoFarResults, QAPInstance &instance) {

	//Inicialización
	QAPSolution initialSolution(instance);
	QAPSimulatedAnnealing sa;
	QAPStopCondition stopCond;
	QAPEvaluator::resetNumEvaluations();
	sa.initialise(0.95, 10, 0.995, 20, instance);
	stopCond.setConditions(MAX_SOLUTIONS_PER_RUN, MAX_ITERATIONS, MAX_SECONS_PER_RUN);

	//Generar solución aleatoria
	QAPSolGenerator::genRandomSol(instance, initialSolution);
	double currentFitness = QAPEvaluator::computeFitness(instance,
			initialSolution);
	initialSolution.setFitness(currentFitness);
	double bestFitness = currentFitness;
	currentResults.push_back(currentFitness);
	bestSoFarResults.push_back(bestFitness);
	sa.setSolution(&initialSolution);

	//Aplicar SA
	sa.run(stopCond);

	//Almacenar los resultados
	vector<double> &resultsSA = sa.getResults();

	for (auto aResult : resultsSA) {
		currentResults.push_back(aResult);
		bestSoFarResults.push_back(min(bestSoFarResults.back(), aResult));
	}

}

/**
 * Función que ejecuta la búsqueda tabú, durante MAX_SECONS_PER_RUN segundos, un máximo de MAX_SOLUTIONS_PER_RUN, para la instancia proporcionada
 * @param[out] currentResults vector donde se almacenarán los valores fitness de las soluciones que va aceptando el enfriamiento simulado.
 * @param[out] bestSoFarResults vector donde se almacenarán los mejores valores fitness generados hasta el momento
 * @param[in] instance Instancia del problema de la mochila cuadrática múltiple
 */
void runATSExperiment(vector<double> &currentResults,
		vector<double> &bestSoFarResults, QAPInstance &instance) {

	//Inicialización
	QAPSolution initialSolution(instance);
	QAPTabuSearch ts;
	QAPStopCondition stopCond;
	QAPEvaluator::resetNumEvaluations();
	ts.initialise(&instance, ((unsigned)(instance.getSize() / 2.5)));
	stopCond.setConditions(MAX_SOLUTIONS_PER_RUN, MAX_ITERATIONS, MAX_SECONS_PER_RUN);

	//Generar solución aleatoria
	QAPSolGenerator::genRandomSol(instance, initialSolution);
	double currentFitness = QAPEvaluator::computeFitness(instance,
			initialSolution);
	initialSolution.setFitness(currentFitness);
	double bestFitness = currentFitness;
	currentResults.push_back(currentFitness);
	bestSoFarResults.push_back(bestFitness);
	ts.setSolution(&initialSolution);

	//Aplicar TS
	ts.run(stopCond);

	//Almacenar los resultados
	vector<double> &resultsTS = ts.getResults();

	for (auto aResult : resultsTS) {
		currentResults.push_back(aResult);
		bestSoFarResults.push_back(min(bestSoFarResults.back(), aResult));
	}
}

/**
 * Función que ejecuta la búsqueda GRASP, durante MAX_SECONS_PER_RUN segundos, un máximo de MAX_SOLUTIONS_PER_RUN, para la instancia proporcionada
 * @param[out] currentResults vector donde se almacenarán los valores fitness de las soluciones que va aceptando el enfriamiento simulado.
 * @param[out] bestSoFarResults vector donde se almacenarán los mejores valores fitness generados hasta el momento
 * @param[in] instance Instancia del problema de la mochila cuadrática múltiple
 */
void runAGraspExperiment(vector<double> &currentResults,
		vector<double> &bestSoFarResults, QAPInstance &instance) {

	//Inicialización
	QAPSolution initialSolution(instance);
	QAPGrasp grasp;
	QAPStopCondition stopCond;
	QAPEvaluator::resetNumEvaluations();
	grasp.initialise(0.25, instance);
	stopCond.setConditions(MAX_SOLUTIONS_PER_RUN, MAX_ITERATIONS, MAX_SECONS_PER_RUN);

	//Generar solución aleatoria para inicialiar la mejor solución
	QAPSolGenerator::genRandomSol(instance, initialSolution);
	double currentFitness = QAPEvaluator::computeFitness(instance,
			initialSolution);
	initialSolution.setFitness(currentFitness);
	double bestFitness = currentFitness;
	currentResults.push_back(currentFitness);
	bestSoFarResults.push_back(bestFitness);

	//Aplicar GRASP
	grasp.run(stopCond);

	//Almacenar los resultados
	vector<double> &resultsGRASP = grasp.getResults();

	for (auto aResult : resultsGRASP) {
		currentResults.push_back(aResult);
		bestSoFarResults.push_back(min(bestSoFarResults.back(), aResult));
	}
}

/**
 * Función que ejecuta la búsqueda Iterated Greedy, durante MAX_SECONS_PER_RUN segundos, un máximo de MAX_SOLUTIONS_PER_RUN, para la instancia proporcionada
 * @param[out] currentResults vector donde se almacenarán los valores fitness de las soluciones que va aceptando el enfriamiento simulado.
 * @param[out] bestSoFarResults vector donde se almacenarán los mejores valores fitness generados hasta el momento
 * @param[in] instance Instancia del problema de la mochila cuadrática múltiple
 */
void runAIGExperiment(vector<double> &currentResults,
		vector<double> &bestSoFarResults, QAPInstance &instance) {

	//Inicialización
	QAPSolution initialSolution(instance);
	QAPIteratedGreedy ig;
	QAPStopCondition stopCond;
	QAPEvaluator::resetNumEvaluations();
	ig.initialise(0.25, instance);
	stopCond.setConditions(MAX_SOLUTIONS_PER_RUN, MAX_ITERATIONS, MAX_SECONS_PER_RUN);

	//Generar solución aleatoria para inicialiar la mejor solución
	QAPSolGenerator::genRandomSol(instance, initialSolution);
	double currentFitness = QAPEvaluator::computeFitness(instance,
			initialSolution);
	initialSolution.setFitness(currentFitness);
	double bestFitness = currentFitness;
	currentResults.push_back(currentFitness);
	bestSoFarResults.push_back(bestFitness);

	//Aplicar IG
	ig.run(stopCond);

	//Almacenar los resultados
	vector<double> &resultsIG = ig.getResults();

	for (auto aResult : resultsIG) {
		currentResults.push_back(aResult);
		bestSoFarResults.push_back(min(bestSoFarResults.back(), aResult));
	}
}
/**
 * Función que ejecuta todos los experimentos para los argumentos pasados al programa principal
 * @param[out] results matriz 3D donde se almacenarán los resultados. El primer índice será para la instancia considerada. El segundo para el experimento sobre dicha instancia. El tercero para la solución generada en dicho experimento
 * @param[in] mainArgs Argumentos de la función main (argv). En los argumentos vienen, desde el índice 1, <nombre del fichero de la instancia 1> <número de mochilas> <nombre del fichero de la instancia 2> <número de mochilas>...
 * @param[in] numMainArgs Número de argumentos de la función main (argc)
 */
void runExperiments(vector<vector<vector<double>*>*> &results, char **mainArgs,
		unsigned int numMainArgs) {

	//Para cada instancia del problema
	for (unsigned int iInstance = 1; iInstance < numMainArgs; iInstance++) {

		//Leer la instancia y crear la estructuras de datos necesarias para guardar los resultados
		QAPInstance instance;
		vector<vector<double>*>* resultsOnThisInstance = new vector<
				vector<double>*>;
		results.push_back(resultsOnThisInstance);
		char *instanceName = mainArgs[iInstance];
		instance.readInstance(instanceName);

		//Ejecutar el enfriamientoSimulado
		vector<double> *theseFirstResults = new vector<double>;
		vector<double> *bestFirstResults = new vector<double>;
		resultsOnThisInstance->push_back(theseFirstResults);
		resultsOnThisInstance->push_back(bestFirstResults);
		runASAExperiment(*theseFirstResults, *bestFirstResults, instance);

		//Ejecutar la búsqueda tabú
		theseFirstResults = new vector<double>;
		bestFirstResults = new vector<double>;
		resultsOnThisInstance->push_back(theseFirstResults);
		resultsOnThisInstance->push_back(bestFirstResults);
		runATSExperiment(*theseFirstResults, *bestFirstResults, instance);

		//Ejecutar la búsqueda GRASP
		theseFirstResults = new vector<double>;
		bestFirstResults = new vector<double>;
		resultsOnThisInstance->push_back(theseFirstResults);
		resultsOnThisInstance->push_back(bestFirstResults);
		runAGraspExperiment(*theseFirstResults, *bestFirstResults, instance);

		//Ejecutar la búsqueda Iterated Greedy
		theseFirstResults = new vector<double>;
		bestFirstResults = new vector<double>;
		resultsOnThisInstance->push_back(theseFirstResults);
		resultsOnThisInstance->push_back(bestFirstResults);
		runAIGExperiment(*theseFirstResults, *bestFirstResults, instance);
	}
}

/**
 * Función que libera la memoria de un vector 2D
 * @param[in] array vector 2D que se va a liberar de memoria
 */
template<typename T>
void free2Darray(vector<vector<T>*> &array) {

	int numVectors = ((int)(array.size()));

	for (int i = 0; i < numVectors; i++) {
		array.at(i)->clear();
		delete (array.at(i));
	}

	array.clear();
}

/**
 * Función que libera la memoria de un vector 3D
 * @param[in] array matriz 3D que se va a liberar de memoria
 */
template<typename T>
void free3Darray(vector<vector<vector<T>*>*> &array) {

	int numArrays = (int)(array.size());

	for (int i = 0; i < numArrays; i++) {
		free2Darray(*(array.at(i)));
		delete (array.at(i));
	}

	array.clear();
}

/**
 * Función main que ejecuta la experimentación. Optimización mediante metaheurísticas basadas en trayectorias, búsquedas con enfriamiento simulado, búsqueda tabú, GRASP e iterated greedy para las instancias.
 *
 * Finalmente se escribe en la salida estandar los resultados generados en forma de matriz. Por cada instancia se generan ocho columnas, una con los resultados
 * que va aceptando cada metaheurística en cada momento y otra con los mejores resultados hasta el momento (para cada metaheurística)
 */
int main(int argc, char **argv) {

	if (argc < 2) {
		cout << argv[0] << " (<problem instance files>)+"
				<< endl;
		return 0;
	}

	//////////////////////////
	//Ejecución de experimentos
	//////////////////////////
	unsigned int numInstances = argc - 1;
	vector<vector<vector<double>*>*> allTheResults;
	srand(seeds[0]);
	//srand(time(NULL));
	//En la matriz allTheResults se almacenarán todos los valores de fitness generados
	//Es tridimensional
	//El primer índice recorre las instancias de los problemas que se han abordado
	//El segundo índice recorre las ejecuciones que se han hecho sobre la misma instancia
	//El tercer índice recorre las soluciones que se han generado en la misma ejecución

	runExperiments(allTheResults, argv, argc);






	//////////////////////
	// Impresión de resultados
	//////////////////////
	vector<double> lastResults;
	for (unsigned int iInstance = 0; iInstance < numInstances; iInstance++){
		cout << argv[iInstance+1] << "_currentSA\t";
		cout << argv[iInstance+1] << "_bestSA\t";
		cout << argv[iInstance+1] << "_currentTS\t";
		cout << argv[iInstance+1] << "_bestTS\t";
		cout << argv[iInstance+1] << "_currentGrasp\t";
		cout << argv[iInstance+1] << "_bestGrasp\t";
		cout << argv[iInstance+1] << "_currentIG\t";
		cout << argv[iInstance+1] << "_bestIG\t";
		lastResults.push_back(allTheResults.at(iInstance)->at(0)->at(0));
		lastResults.push_back(allTheResults.at(iInstance)->at(1)->at(0));
		lastResults.push_back(allTheResults.at(iInstance)->at(2)->at(0));
		lastResults.push_back(allTheResults.at(iInstance)->at(3)->at(0));
		lastResults.push_back(allTheResults.at(iInstance)->at(4)->at(0));
		lastResults.push_back(allTheResults.at(iInstance)->at(5)->at(0));
		lastResults.push_back(allTheResults.at(iInstance)->at(6)->at(0));
		lastResults.push_back(allTheResults.at(iInstance)->at(7)->at(0));
	}
	cout << endl;
	bool allResultsPrinted = false;
	unsigned int iIteration = 0;
	unsigned int indexColumn = 0;

	while (allResultsPrinted == false){
		allResultsPrinted = true;
		for (unsigned int iInstance = 0; iInstance < numInstances; iInstance++){

			if (allTheResults.at(iInstance)->at(0)->size() > iIteration){
				allResultsPrinted = false;
				cout << allTheResults.at(iInstance)->at(0)->at(iIteration) << "\t";
				lastResults[indexColumn] = allTheResults.at(iInstance)->at(0)->at(iIteration);
			} else {
				cout << lastResults[indexColumn] << "\t";
			}
			indexColumn++;

			if (allTheResults.at(iInstance)->at(0)->size() > iIteration){
				allResultsPrinted = false;
				cout << allTheResults.at(iInstance)->at(1)->at(iIteration) << "\t";
				lastResults[indexColumn] = allTheResults.at(iInstance)->at(1)->at(iIteration);
			} else {
				cout << lastResults[indexColumn] << "\t";
			}
			indexColumn++;

			if (allTheResults.at(iInstance)->at(2)->size() > iIteration){
				allResultsPrinted = false;
				cout << allTheResults.at(iInstance)->at(2)->at(iIteration) << "\t";
				lastResults[indexColumn] = allTheResults.at(iInstance)->at(2)->at(iIteration);
			} else {
				cout << lastResults[indexColumn] << "\t";
			}
			indexColumn++;

			if (allTheResults.at(iInstance)->at(3)->size() > iIteration){
				allResultsPrinted = false;
				cout << allTheResults.at(iInstance)->at(3)->at(iIteration) << "\t";
				lastResults[indexColumn] = allTheResults.at(iInstance)->at(3)->at(iIteration);
			} else {
				cout << lastResults[indexColumn] << "\t";
			}
			indexColumn++;

			if (allTheResults.at(iInstance)->at(4)->size() > iIteration){
				allResultsPrinted = false;
				cout << allTheResults.at(iInstance)->at(4)->at(iIteration) << "\t";
				lastResults[indexColumn] = allTheResults.at(iInstance)->at(4)->at(iIteration);
			} else {
				cout << lastResults[indexColumn] << "\t";
			}
			indexColumn++;

			if (allTheResults.at(iInstance)->at(5)->size() > iIteration){
				allResultsPrinted = false;
				cout << allTheResults.at(iInstance)->at(5)->at(iIteration) << "\t";
				lastResults[indexColumn] = allTheResults.at(iInstance)->at(5)->at(iIteration);
			} else {
				cout << lastResults[indexColumn] << "\t";
			}
			indexColumn++;

			if (allTheResults.at(iInstance)->at(6)->size() > iIteration){
				allResultsPrinted = false;
				cout << allTheResults.at(iInstance)->at(6)->at(iIteration) << "\t";
				lastResults[indexColumn] = allTheResults.at(iInstance)->at(6)->at(iIteration);
			} else {
				cout << lastResults[indexColumn] << "\t";
			}
			indexColumn++;

			if (allTheResults.at(iInstance)->at(7)->size() > iIteration){
				allResultsPrinted = false;
				cout << allTheResults.at(iInstance)->at(7)->at(iIteration) << "\t";
				lastResults[indexColumn] = allTheResults.at(iInstance)->at(7)->at(iIteration);
			} else {
				cout << lastResults[indexColumn] << "\t";
			}
			indexColumn++;
		}
		cout << endl;
		iIteration++;
		indexColumn = 0;
	}




	//////////////////////
	// Liberar memoria
	//////////////////////
	free3Darray(allTheResults);
	return 0;
}

#endif

#ifdef P4

//Definición de algunos parámetros de la experimentación
#define MAX_SECONS_PER_RUN 0
#define MAX_SOLUTIONS_PER_RUN 15000
#define MAX_ITERATIONS 0

#include "QAPEvaluator.h"
#include "QAPInstance.h"
#include "QAPSolGenerator.h"
#include "QAPSolution.h"
#include "QAPGeneticAlgorithm.h"
#include "QAPAntColonyOpt.h"
#include "QAPStopCondition.h"
#include "Timer.h"
#include "QAPLocalSearch.h"
#include "QAPSimpleFirstImprovementNO.h"

#include <iostream>
using namespace std;

//Definición externa de las semillas para el generador de números aleatorios (en seeds.cpp)
//extern unsigned int numSeeds;
extern unsigned int seeds[];

/**
 * Función que ejecuta el algoritmo evolutivo, durante MAX_SECONS_PER_RUN segundos, un máximo de MAX_SOLUTIONS_PER_RUN, para la instancia proporcionada
 * @param[out] currentResults vector donde se almacenarán los valores fitness de las soluciones que se van generando.
 * @param[out] bestSoFarResults vector donde se almacenarán los mejores valores fitness generados hasta el momento
 * @param[out] bestPerIterations vector donde se almacenarán los mejores valores fitness de cada generación
 * @param[out] popMean vector donde se almacenarán los valores fitness medios de cada generación
 * @param[out] offMean vector donde se almacenarán los valores fitness medios de la población de descendientes
 * @param[in] instance Instancia del problema de la mochila cuadrática múltiple
 */
#define GA_WITH_LS
void runAGAExperiment(vector<double> &currentResults,
		vector<double> &bestSoFarResults, vector<double> &bestPerIterations,
		vector<double> &popMean, vector<double> &offMean,
		QAPInstance &instance) {

	//Inicialización
	QAPGeneticAlgorithm ga;
	QAPStopCondition stopCond;
	QAPEvaluator::resetNumEvaluations();
	ga.initialise(120, instance);
	stopCond.setConditions(MAX_SOLUTIONS_PER_RUN, MAX_ITERATIONS, MAX_SECONS_PER_RUN);

	//Ejecutar el GA
	ga.run(stopCond);

	//Almacenar los resultados
	vector<double> &resultsGA = ga.getResults();

	for (double aResult : resultsGA) {
		currentResults.push_back(aResult);

		if (bestSoFarResults.size() > 0)
			bestSoFarResults.push_back(min(bestSoFarResults.back(), aResult));
		else
			bestSoFarResults.push_back(aResult);
	}

	bestPerIterations = ga.getBestsPerIterations();
	popMean = ga.getPopMeanResults();
	offMean = ga.getOffMeanResults();

#ifdef GA_WITH_LS
	QAPSolution bestSolution = *(ga.getBestSolution());
	QAPLocalSearch ls;
	QAPSimpleFirstImprovementNO no;

	ls.optimise(instance, no, bestSolution);

	vector<double> &resultsLS = ls.getResults();

	for(double aResult : resultsLS) {
		currentResults.push_back(aResult);
		if (bestSoFarResults.size() > 0)
			bestSoFarResults.push_back(min(bestSoFarResults.back(), aResult));
		else
			bestSoFarResults.push_back(aResult);
	}
#endif

}
#define ACO_WITH_LS

/**
 * Función que ejecuta el sistema de colonia de hormigas, durante MAX_SECONS_PER_RUN segundos, un máximo de MAX_SOLUTIONS_PER_RUN, para la instancia proporcionada
 * @param[out] currentResults vector donde se almacenarán los valores fitness de las soluciones que se van generando.
 * @param[out] bestSoFarResults vector donde se almacenarán los mejores valores fitness generados hasta el momento
 * @param[out] bestPerIterations vector donde se almacenarán los mejores valores fitness de cada iteracion del algoritmo
 * @param[out] antsMean vector donde se almacenarán los valores fitness medios de cada iteración del algoritmo
 * @param[in] instance Instancia del problema de la mochila cuadrática múltiple
 */
void runAnACOExperiment(vector<double> &currentResults,
		vector<double> &bestSoFarResults, vector<double> &bestPerIterations,
		vector<double> &antsMean, QAPInstance &instance) {

	//Inicialización
	QAPAntColonyOpt aco;
	QAPStopCondition stopCond;
	QAPEvaluator::resetNumEvaluations();
	aco.initialise(5, 0.1, 1, 0.5, 0.1, 0.1, 20, instance);
	stopCond.setConditions(MAX_SOLUTIONS_PER_RUN, MAX_ITERATIONS, MAX_SECONS_PER_RUN);

	//Ejecutar el ACO
	aco.run(stopCond);

	//Almacenar los resultados
	vector<double> &resultsACO = aco.getResults();

	for (double aResult : resultsACO) {
		currentResults.push_back(aResult);

		if (bestSoFarResults.size() > 0)
			bestSoFarResults.push_back(min(bestSoFarResults.back(), aResult));
		else
			bestSoFarResults.push_back(aResult);
	}

	bestPerIterations = aco.getBestPerIteration();
	antsMean = aco.getAntsMeanResults();

#ifdef ACO_WITH_LS
	QAPSolution bestSolution = *(aco.getBestSolution());
	QAPLocalSearch ls;
	QAPSimpleFirstImprovementNO no;

	ls.optimise(instance, no, bestSolution);

	vector<double> &resultsLS = ls.getResults();

	for(double aResult : resultsLS) {
		currentResults.push_back(aResult);
		if (bestSoFarResults.size() > 0)
			bestSoFarResults.push_back(min(bestSoFarResults.back(), aResult));
		else
			bestSoFarResults.push_back(aResult);
	}
#endif
}

/**
 * Función que ejecuta todos los experimentos para los argumentos pasados al programa principal
 * @param[out] results matriz 3D donde se almacenarán los resultados. El primer índice será para la instancia considerada. El segundo para el experimento sobre dicha instancia (y posibles subcategorías: resultado actual, mejor hasta el momento...). El tercero para la solución generada en dicho experimento
 * @param[in] mainArgs Argumentos de la función main (argv). En los argumentos vienen, desde el índice 1, <nombre del fichero de la instancia 1> <número de mochilas> <nombre del fichero de la instancia 2> <número de mochilas>...
 * @param[in] numMainArgs Número de argumentos de la función main (argc)
 */
void runExperiments(vector<vector<vector<double>*>*> &results, char **mainArgs,
		unsigned int numMainArgs) {

	//Para cada instancia del problema
	for (unsigned int iInstance = 1; iInstance < numMainArgs; iInstance++) {

		//Leer la instancia y crear la estructuras de datos necesarias para guardar los resultados
		QAPInstance instance;
		vector<vector<double>*>* resultsOnThisInstance = new vector<
				vector<double>*>;
		results.push_back(resultsOnThisInstance);
		char *instanceName = mainArgs[iInstance];
		instance.readInstance(instanceName);

		//Ejecutar el algoritmo evolutivo
		vector<double> *theseResults = new vector<double>;
		vector<double> *bestResults = new vector<double>;
		vector<double> *bestPerIterations = new vector<double>;
		vector<double> *popMeanResults = new vector<double>;
		vector<double> *offMeanResults = new vector<double>;
		resultsOnThisInstance->push_back(theseResults);
		resultsOnThisInstance->push_back(bestResults);
		resultsOnThisInstance->push_back(bestPerIterations);
		resultsOnThisInstance->push_back(popMeanResults);
		resultsOnThisInstance->push_back(offMeanResults);
		runAGAExperiment(*theseResults, *bestResults, *bestPerIterations,
				*popMeanResults, *offMeanResults, instance);

		//Ejecutar el algoritmo de colonia de hormigas
		theseResults = new vector<double>;
		bestResults = new vector<double>;
		bestPerIterations = new vector<double>;
		vector<double> *antsMean = new vector<double>;
		resultsOnThisInstance->push_back(theseResults);
		resultsOnThisInstance->push_back(bestResults);
		resultsOnThisInstance->push_back(bestPerIterations);
		resultsOnThisInstance->push_back(antsMean);
		runAnACOExperiment(*theseResults, *bestResults, *bestPerIterations,
				*antsMean, instance);
	}
}

/**
 * Función que libera la memoria de un vector 2D
 * @param[in] array vector 2D que se va a liberar de memoria
 */
template<typename T>
void free2Darray(vector<vector<T>*> &array) {

	int numVectors = ((int) (array.size()));

	for (int i = 0; i < numVectors; i++) {
		array.at(i)->clear();
		delete (array.at(i));
	}

	array.clear();
}

/**
 * Función que libera la memoria de un vector 3D
 * @param[in] array matriz 3D que se va a liberar de memoria
 */
template<typename T>
void free3Darray(vector<vector<vector<T>*>*> &array) {

	int numArrays = (int) (array.size());

	for (int i = 0; i < numArrays; i++) {
		free2Darray(*(array.at(i)));
		delete (array.at(i));
	}

	array.clear();
}





/**
 * Función main que ejecuta la experimentación. Optimización mediante metaheurísticas basadas en poblaciones, algoritmo genético y algoritmo de colonias de hormigas para las instancias.
 *
 * Finalmente se escribe en la salida estandar los resultados generados en forma de matriz.
 *
 * TODO LEER: En este caso, la salida podéis configurarla según vuestro interés (ver variable printColumns). Actualmente se podrían imprimir hasta 9 columnas por instancia:
 *  - El valor fitness de la última solución generada por el Genético (currentGA),
 *  - El mejor valor fitness hasta el momento del Genétic (bestGA),
 *  - El valor fitness de la mejor solución de la población, por iteraciones (bestGAPerIt),
 *  - La media de los valores fitness de la población, por iteraciones (popMeanGA),
 *  - La media de los valores fitness de los descendientes generados, por iteraciones (offMeanGA),
 *
 *  - El valor fitness de la última solución generada por el ACO (currentACO),
 *  - El mejor valor fitness hasta el momento del ACO (bestACO),
 *  - El valor fitness de la mejor solución generada en la iteración actual, por iteraciones (bestACOPerIt),
 *  - La media de los valores fitness de las soluciones generadas por las hormigas, por iteraciones (antsMeanACO)
 *
 *  NO se aconseja imprimir todas las columnas a la vez (o en tal caso, editar el código para que vayan a ficheros diferentes). El problema es que el número de datos en las
 *  columnas "por iteraciones" es menor que el número de datos en las otras columnas, y dado que el programa equilibra el númerdo de datos repitiendo el último, una representación
 *  conjunta de los datos en la misma gráfica puede no ser ilustrativa del comportamiento del algoritmo.
 */
int main(int argc, char **argv) {

	if (argc < 2) {
		cout << argv[0] << " (<problem instance files>)+"
				<< endl;
		return 0;
	}



	//////////////////////////
	//Ejecución de experimentos
	//////////////////////////
	unsigned int numInstances = (argc - 1);
	vector<vector<vector<double>*>*> allTheResults;
	srand(seeds[0]);
	//En la matriz allTheResults se almacenarán todos los valores de fitness generados
	//Es tridimensional
	//El primer índice recorre las instancias de los problemas que se han abordado
	//El segundo índice recorre los experimentos que se han hecho sobre la misma instancia (y posibles subcategorías: resultado actual, mejor hasta el momento...)
	//El tercer índice recorre cada resultado

	runExperiments(allTheResults, argv, argc);




	//////////////////////
	// Impresión de resultados
	//////////////////////
	vector<double> lastResults;
//	vector<unsigned> printColumns = { 2, 3, 4, 7, 8};
	vector<unsigned> printColumns = { 0, 1, 5, 6 };
	vector<string> titles =
			{ "currentGA", "bestGA", "bestGAPerIt", "popMeanGA", "offMeanGA",
					"currentACO", "bestACO", "bestACOPerIt", "antsMeanACO" };
	for (unsigned int iInstance = 0; iInstance < numInstances; iInstance++) {

		for (auto indexColumnPerInstance : printColumns) {
			cout << argv[iInstance + 1] << "_" << titles[indexColumnPerInstance] << "\t";
			lastResults.push_back(
					allTheResults.at(iInstance)->at(indexColumnPerInstance)->at(
							0));
		}
	}
	cout << endl;
	bool allResultsPrinted = false;
	unsigned int iIteration = 0;
	unsigned int indexColumn = 0;

	while (allResultsPrinted == false) {
		allResultsPrinted = true;
		for (unsigned int iInstance = 0; iInstance < numInstances;
				iInstance++) {

			for (auto indexColumnPerInstance : printColumns) {

				if (allTheResults.at(iInstance)->at(indexColumnPerInstance)->size()
						> iIteration) {
					allResultsPrinted = false;
					cout
							<< allTheResults.at(iInstance)->at(
									indexColumnPerInstance)->at(iIteration)
							<< "\t";
					lastResults[indexColumn] = allTheResults.at(iInstance)->at(
							indexColumnPerInstance)->at(iIteration);
				} else {
					cout << lastResults[indexColumn] << "\t";
				}
				indexColumn++;
			}
		}
		cout << endl;
		iIteration++;
		indexColumn = 0;
	}



	//////////////////////
	// Liberar memoria
	//////////////////////
	free3Darray(allTheResults);
	return 0;
}

#endif
