
/*
 * QAPSolution.cpp
 *
 *  Created on: 20 feb. 2017
 */

#include "QAPSolution.h"
#include "QAPInstance.h"

QAPSolution::QAPSolution(QAPInstance &instance) {
	// Inicializar las variables miembro
	_sol.resize(instance.getSize());
	for(int i = 0; i < instance.getSize(); i++)
	{
		//Asignamos el departamento i a la localización i.
		_sol.at(i) = i;
	}

	_size = instance.getSize();
	_fitness = instance.getSumDistanceTimesFlow(*this);
	_fitnessAssigned = false;
}

QAPSolution::~QAPSolution() {}

void QAPSolution::assignFacilityToLocation(int facility, int location)
{
	//Asignar un lugar a un departamento en el vector solución.
	_sol.at(facility) = location;
}

int QAPSolution::whereIsFacility(int facility) const
{
	//Devolver dónde se encuentra un departamento.
	return _sol.at(facility);
}

int QAPSolution::whichFacilityIsThere(int location)
{
	/*
	 * Buscamos el departamento que se encuentra en la localización location.
	 */
	for(int i = 0; i < _size; i++)
	{
		if(whereIsFacility(i) == location)
			return i;
	}
	return -1;
}

double QAPSolution::getFitness() const {
	return _fitness;
}

void QAPSolution::setFitness(double fitness) {
	_fitness = fitness;
	_fitnessAssigned = true;
}

void QAPSolution::copy(Solution& solution) {

	/*
	 * 1. Copiar asignación de departamentos a lugares.
	 * 2. Copiar el fitness de la solución.
	 */

	QAPSolution &sol = (QAPSolution&)solution;

	for(int i = 0; i < _size; i++)
	{
		assignFacilityToLocation(i, sol.whereIsFacility(i));
	}

	setFitness(solution.getFitness());

}

bool QAPSolution::hasValidFitness()
{
	return _fitnessAssigned;
}
