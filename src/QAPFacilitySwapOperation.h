/*
 * QAPFacilitySwapOperation.h
 *
 *  Created on: 9 mar. 2017
 */

#ifndef SRC_QAPFACILITYSWAPOPERATION_H_
#define SRC_QAPFACILITYSWAPOPERATION_H_

#include "QAPChangeOperation.h"
#include "QAPSolution.h"

class QAPFacilitySwapOperation : public QAPChangeOperation{

protected:
	int _indexFacility1;
	int _indexFacility2;
	double _deltaFitness;

public:

	QAPFacilitySwapOperation();

	virtual ~QAPFacilitySwapOperation();

	virtual void apply(QAPSolution &solution);

	void setValues(int indexFacility1, int indexFacility2, double deltaFitness);

	double getDeltaFitness(){
		return _deltaFitness;
	}

	std::pair<unsigned,unsigned> getFac(QAPSolution &sol){
		std::pair<unsigned,unsigned> movement;
		movement = std::make_pair((unsigned)_indexFacility1,(unsigned) sol.whereIsFacility(_indexFacility1));
		return movement;
	}

	std::pair<unsigned,unsigned> getInvFac(QAPSolution &sol){
		std::pair<unsigned,unsigned> movement;
		movement = std::make_pair((unsigned)_indexFacility2,(unsigned) sol.whereIsFacility(_indexFacility2));
		return movement;
	}

	int getFacility1()
	{
		return _indexFacility1;
	}

	int getFacility2()
	{
		return _indexFacility2;
	}

};

#endif /* SRC_QAPFACILITYSWAPOPERATION_H_ */
