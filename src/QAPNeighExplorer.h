/*
 * QAPNeighExplorer.h
 *
 *  Created on: 9 mar. 2017
 *      Author: victor
 */

#ifndef SRC_QAPNEIGHEXPLORER_H_
#define SRC_QAPNEIGHEXPLORER_H_

#include "QAPSolution.h"
#include "QAPInstance.h"
#include "QAPChangeOperation.h"

/**
 * clase abstracta que define las operaciones de cualquier operador que explora la vecindad de una solución dada.
 */
class QAPNeighExplorer {
public:


	virtual ~QAPNeighExplorer(){}

	virtual bool findOperation(QAPInstance &instance, QAPSolution &solution, QAPChangeOperation &operation) = 0;
};



#endif /* SRC_QAPNEIGHEXPLORER_H_ */
