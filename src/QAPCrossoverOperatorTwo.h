/*
 * QAPCrossoverOperatorTwo.h
 *
 * Fichero que define la clase QAPCrossoverOperator. Forma parte del código esqueleto para el problema de las múltiples mochilas cuadráticas, ofrecido para las prácticas de la asignatura Metaheurísticas del Grado de Ingeniería Informática de la Universidad de Córdoba
 *
 * @author Carlos García cgarcia@uco.es
 */

#ifndef INCLUDE_QAPCROSSOVEROPERATORTWO_H_
#define INCLUDE_QAPCROSSOVEROPERATORTWO_H_

#include "QAPSolution.h"
#include "Solution.h"
#include "QAPInstance.h"
#include <vector>
#include <algorithm> //min, max
#include <iostream>
#include "QAPCrossoverOperator.h"

using namespace std;

/**
 * Clase que implementa un operador de cruce uniforme para el problema QAP.
 */
class QAPCrossoverOperatorTwo : public QAPCrossoverOperator {

protected:
	/**
	 * Variables miembro de la clase:
	 *  - _instance Instancia de problema abordada. Se utiliza únicamente para crear nuevos objetos QAPSolution
	 *  - _numObjs almacena el tamaño de la instancia
	 *  - _crossProb probabilidad de cruce
	 */
	QAPInstance *_instance;
	unsigned _size;
	double _crossProb;

	/**
	 * Función que cruza dos soluciones según su probabilidad de cruce. En caso de que no haya crucce, la solución devuelta será igual al primer padre
	 * @param[in] s1 primer padre
	 * @param[in] s2 segundo padre
	 * @return Nuevo objeto solución descendiente de haber cruzado s1 y s2. La solución se reserva dinámicamente en memoria. Es responsabilidad del invocador de gestionarla correctamente.
	 */
	void cross(Solution *s1, Solution *s2, QAPSolution * c1, QAPSolution * c2) {
		QAPSolution * sol1 = (QAPSolution *) s1;
		QAPSolution * sol2 = (QAPSolution *) s2;

		//Una probabilidad == 1 haría que el algoritmo convergiese demasiado rápido
		double randSample = (((double) rand()) / RAND_MAX);

		if (randSample < _crossProb) {
			/*
			 * El cruce consistirá en seleccionar un conjunto de departamentos consecutivos del padre 1 e
			 * insertarlos en la nueva solución. Estos elementos se marcarán en el padre 2 para que no se
			 * vuelvan a seleccionar. Los departamentos restantes se tomarán del padre 2 y se empezará a
			 * rellenar por la derecha.
			 */
			int a = rand() % _size;
			int b = rand() % _size;
			int index1 = min(a,b);
			int index2 = max(a,b);

			bool isSelected1 = false;
			bool isSelected2 = false;

			vector<int> selected;
			for(int i = index1; i <= index2; i++)
			{
				//int Facility1= sol1->whereIsFacility(i);
				//int Facility2= sol2->whereIsFacility(i);
				c1->assignFacilityToLocation(i, sol2->whereIsFacility(i));
				c2->assignFacilityToLocation(i, sol1->whereIsFacility(i));
				selected.push_back(sol1->whereIsFacility(i));
				selected.push_back(sol2->whereIsFacility(i));
				//Marcamos con el valor de location -10 los departamentos en el padre 2.
				//int iF = auxSol2->whichFacilityIsThere(iL);
				//auxSol2->assignFacilityToLocation(iF, -10);
			}
			for(int i = 0; i < (int)_size ; i++){
				if(i >= index1 && i <= index2)
					continue;
				int loc1= sol1->whereIsFacility(i);
				int loc2= sol2->whereIsFacility(i);

				for(unsigned j = 0; j < selected.size(); j++){
					if(isSelected2 == false)
					{
						if(selected[j] == loc2)
							isSelected2 = true;
					}
					else{
						if(isSelected1 == true) //Si ambos est�n en los seleccionados en el primer bucle
						{break;}	//Dejar de comprobar si est�n seleccionados y pasar al siguiente Departamento de las soluciones.
					}

					if(isSelected1 == false)
						if(selected[j] == loc1)
							isSelected1 = true;
				}

				if(isSelected1 == false){
					c1->assignFacilityToLocation(i, sol1->whereIsFacility(i));
				}
				else{
					c1->assignFacilityToLocation(i, -10);
					isSelected1 = false;
				}
				if(isSelected2 == false){
					c2->assignFacilityToLocation(i, sol2->whereIsFacility(i));
				}
				else{
					c2->assignFacilityToLocation(i, -10);
					isSelected2 = false;
				}
			}

			for(int i = 0; i< (int)_size; i++)
			{
				if(c1->whereIsFacility(i) == -10)
				{
					for(int j=0; j< (int)_size; j++)
					{
						if(c2->whereIsFacility(j) == -10)
						{
							c2->assignFacilityToLocation(j, sol1->whereIsFacility(i));
							c1->assignFacilityToLocation(i, sol2->whereIsFacility(j));
							break;
						}
					}
				}
			}


		} else {
			//Si no hay cruce, copiar el primer padre
			c1->copy(*s1);
			c2->copy(*s2);
		}
	}

public:

	/**
	 * Constructor
	 * @param[in] crossProb Probabilidad de cruce
	 * @param[in] instance Instancia del problema abordada
	 */
	QAPCrossoverOperatorTwo(double crossProb, QAPInstance &instance) {
		_instance = &instance;
		_size = instance.getSize();
		_crossProb = crossProb;
	}

	virtual ~QAPCrossoverOperatorTwo(){}

	/**
	 * Función que aplica el cruce a una población de padres.
	 * @param[in] parents Vector de padres. El cruce se aplica entre cada dos padres consecutivos (1,2), (3,4)...
	 * @param[out] offspring Vector donde se almacenan los descendientes generados. IMPORTANTE: Esta función reserva memoria dinámicamente para las nuevas soluciones en offspring, por lo que es responsabilidad de quien la invoque de gestionar la memoria adecuadamente.
	 */
	virtual void cross(vector<Solution*> &parents, vector<Solution*> &offspring) {

		unsigned numParents = (unsigned) parents.size();

		//aplicar cruce entre cada dos padres consecutivos (1,2), (3,4), ...
		for(unsigned i=0; i<numParents-1; i+=2) {
			QAPSolution *sol1, *sol2;
			sol1 = new QAPSolution(*_instance);
			sol2 = new QAPSolution(*_instance);
			cross(parents[i], parents[i+1], sol1, sol2);
			offspring.push_back(sol1);
			offspring.push_back(sol2);
		}
	}
};

#endif /* INCLUDE_QAPCROSSOVEROPERATORTWO_H_ */
