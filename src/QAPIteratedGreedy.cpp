/*
 * QAPIteratedGreedy.cpp
 *
 * Fichero que define las funciones de la clase QAPIteratedGreedy. Forma parte del código esqueleto para el problema de las múltiples mochilas cuadráticas, ofrecido para las prácticas de la asignatura Metaheurísticas del Grado de Ingeniería Informática de la Universidad de Córdoba
 *
 * @author Carlos García cgarcia@uco.es
 */

#include "QAPIteratedGreedy.h"
#include "QAPSolution.h"
#include <iostream>
#include <vector>
#include "QAPFacilityAssignmentOperation.h"

using namespace std;

void QAPIteratedGreedy::chooseOperation(
		QAPFacilityAssignmentOperation& operation) {

	int bestFacility = 0;
	int bestLocation = 0;
	double bestDeltaFitness = 0;
	bool initialisedBestDeltaFitness = false;

	/*
	 * Recorrer todos los departamentos destruidos.
	 * 	Recorrer todas las localizaciones libres.
	 * 	 Calcular el deltaFitness de asignar el departamento a la localización.
	 * 	 Almacenar esa solución en caso de tener el mejor deltaFitness.
	 *
	 */

	for(int i = 0; i < _instance->getSize(); i++)
	{
		if(_sol->whereIsFacility(i) < 0)
		{
			for(int j = 0; j < _instance->getSize(); j++)
			{
				if(_sol->whichFacilityIsThere(j) == -1)
				{
					double deltaFitness = QAPEvaluator::computeDeltaFitnessAssignment(
							*_instance, *_sol, i, j);

					if (deltaFitness < bestDeltaFitness || initialisedBestDeltaFitness == false) {
						initialisedBestDeltaFitness = true;
						bestFacility = i;
						bestLocation = j;
						bestDeltaFitness = deltaFitness;
					}

				}
			}
		}
	}

	operation.setValues(bestFacility, bestLocation, bestDeltaFitness);
}

void QAPIteratedGreedy::rebuild() {

	/** Seleccionar la primera operación */
	QAPFacilityAssignmentOperation operation;
	chooseOperation(operation);

	/**
	 * Mientras la operación tenga un incremento de fitness negativo, operation.getDeltaFitness(),
	 *  1. aplicar la operación en _sol
	 *  2. Almacenar el fitness de la solución en _result (para las gráficas)
	 *  3. seleccionar una nueva operación
	 */

	while (operation.getDeltaFitness() < 0) {
		operation.apply(*_sol);
		_results.push_back(_sol->getFitness());
		chooseOperation(operation);
	}

}

void QAPIteratedGreedy::destroy() {

	/**
	 * Recorrer los departamentos y destruirlos con probabilidad _alpha.
	 */

	unsigned numFacilities = _instance->getSize();

	for (unsigned i = 0; i < numFacilities; i++){

		double randSample = ((double)(rand())) / RAND_MAX;

		if (randSample < _alpha){
			//_destroyedFacilities.push_back(i);
			_sol->assignFacilityToLocation(i, -1);
		}
	}

	double fitness = QAPEvaluator::computeFitness(*_instance, *_sol);
	_sol->setFitness(fitness);
	_results.push_back(_sol->getFitness());
}

void QAPIteratedGreedy::initialise(double alpha, QAPInstance& instance) {
	_sol = new QAPSolution(instance);
	_bestSolution = new QAPSolution(instance);
	_bestSolution->copy(*_sol);
	_instance = &instance;
	_alpha = alpha;
}

void QAPIteratedGreedy::run(QAPStopCondition& stopCondition) {

	if (_sol == NULL) {
		cerr << "IG was not initialised" << endl;
		exit(-1);
	}

	/** Crear la primera solución */
	rebuild();

	if (QAPEvaluator::compare(_sol->getFitness(), _bestSolution->getFitness()) > 0)
		_bestSolution->copy(*_sol);

	/**
	 * Mientras no se alcance la condición de parada
	 *  1. Destruir parcialmente la solución
	 *  2. Reconstruir la solución
	 *  3. Almacenar el nuevo fitness en _results para las gráficas
	 *  4. Actualizar la mejor solución y volver a ella si la nueva es peor
	 */

	while (stopCondition.reached() == false) {
		destroy();
		rebuild();
		_results.push_back(_sol->getFitness());

		if (QAPEvaluator::compare(_sol->getFitness(), _bestSolution->getFitness()) > 0)
		{
			_bestSolution->copy(*_sol);
		}
		else
		{
			_sol->copy(*_bestSolution);
		}
		stopCondition.notifyIteration();
	}

}


