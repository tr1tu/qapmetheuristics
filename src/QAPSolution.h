/*
 * QAPSolution.h
 *
 *  Created on: 20 feb. 2017
 */

#ifndef __QAPSOLUTION_H__
#define __QAPSOLUTION_H__

#include <vector>

#ifndef __QAPINSTANCE_H__
#include "QAPInstance.h"
#else
class QAPInstance;
#endif

#include "Solution.h"

class QAPSolution : public Solution {
protected:
	/* Definir variables miembro.
	 * _size define el numero de lugares/departamentos.
	 * _sol vector de enteros que será la representación del problema.
	 * 		Cada posición del vector se corresponde con un departamento.
	 * 		El valor de cada posición indica a que lugar está asignada.
	 * _fitness valor que almacena la calidad de la solucion.
	 */
	int _size;
	std::vector<int> _sol;
	double _fitness;
	bool _fitnessAssigned;

public:
	QAPSolution(QAPInstance &instance);
	~QAPSolution();


	/*
	 * Función que asigna un departamento a un lugar.
	 */
	void assignFacilityToLocation(int facility, int location);

	/*
	 * Función que devuelve el lugar en el que se encuentra un departamento.
	 */
	int whereIsFacility(int facility) const;

	/*
	 * Función que devuelve qué departamento se encuentra la localización dada.
	 */
	int whichFacilityIsThere(int location);

	/**
	 * Función que devuelve el fitness de la solución
	 *
	 * @return fitness de la solución
	 */
	virtual double getFitness() const;

	/**
	 * Función que asigna el fitness de la solución
	 * @param[in] fitness Fitness de la solución
	 */
	virtual void setFitness(double fitness);

	/**
	 * Función que copia la información de otra solución
	 * @param[in] solution La solución de donde copiar la información
	 */
	virtual void copy(Solution &solution);

	virtual bool hasValidFitness();
};

#endif /* __QAPSOLUTION_H__ */
