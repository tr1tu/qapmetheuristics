/*
 * QAPSimpleBestImprovementNO.h
 *
 *  Created on: 9 mar. 2017
 *      Author: victor
 */

#ifndef SRC_QAPSIMPLEBESTIMPROVEMENTNO_H_
#define SRC_QAPSIMPLEBESTIMPROVEMENTNO_H_

#include "QAPNeighExplorer.h"
#include "QAPInstance.h"
#include "QAPSolution.h"
#include "QAPChangeOperation.h"
#include "QAPEvaluator.h"
#include <stdlib.h>
#include <iostream>
#include <vector>
#include "QAPFacilitySwapOperation.h"

using namespace std;


class QAPSimpleBestImprovementNO : public QAPNeighExplorer {

public:

	virtual ~QAPSimpleBestImprovementNO(){

	}

	virtual bool findOperation(QAPInstance &instance, QAPSolution &solution, QAPChangeOperation &operation);


};

#endif /* SRC_QAPSIMPLEBESTIMPROVEMENTNO_H_ */
