/*
 * QAPSimpleFirstImprovementNO.cpp
 *
 *  Created on: 9 mar. 2017
 *      Author: victor
 */

#include "QAPSimpleFirstImprovementNO.h"
#include <iostream>
bool QAPSimpleFirstImprovementNO::findOperation(QAPInstance &instance, QAPSolution &solution, QAPChangeOperation &operation)
{

	QAPFacilitySwapOperation *oaOperation = dynamic_cast<QAPFacilitySwapOperation*>(&operation);
	if (oaOperation == NULL){
		cerr << "MQKPSimpleFirstImprovementNO::findOperation recibió un objeto operation que no es de la clase MQKPObjectAssignmentOperation" << endl;
		exit(1);
	}

	//Crear una permutación de los índices de los objetos e inicializar algunas variables
	vector<int> perm;
	int numFacilites = instance.getSize();
	QAPInstance::randomPermutation(numFacilites, perm);
	double deltaFitness;

	//std::cout << "Fitness de la solucion: " << solution.getFitness() << std::endl;
	for(int i = 0; i<numFacilites -1 ; i++)
	{
		for(int j = i+1; j<numFacilites; j++)
		{
			deltaFitness = QAPEvaluator::computeDeltaFitnessSwap(instance, solution, perm[i], perm[j]);
			//std::cout << "DeltaFitness >>" << deltaFitness << "<<";
			if(deltaFitness < 0)
			{
				oaOperation->setValues(perm[i], perm[j], deltaFitness);
				return true;
			}

		}
	}

	return false;
}
