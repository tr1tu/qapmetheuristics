/*
 * QAPFacilityAssignmentOperation.h
 *
 *  Created on: 9 mar. 2017
 */

#ifndef SRC_QAPFACILITYASSIGNMENTOPERATION_H_
#define SRC_QAPFACILITYASSIGNMENTOPERATION_H_

#include "QAPChangeOperation.h"
#include "QAPSolution.h"

class QAPFacilityAssignmentOperation : public QAPChangeOperation{

protected:
	int _indexFacility;
	int _indexLocation;
	double _deltaFitness;

public:

	QAPFacilityAssignmentOperation();

	virtual ~QAPFacilityAssignmentOperation();

	virtual void apply(QAPSolution &solution);

	void setValues(int indexFacility, int indexLocation, double deltaFitness);

	double getDeltaFitness(){
		return _deltaFitness;
	}

	int getFacility() const;

	int getLocation() const;

};

#endif /* SRC_QAPFACILITYASSIGNMENTOPERATION_H_ */
