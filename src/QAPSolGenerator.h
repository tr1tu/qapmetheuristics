/*
 * QAPSolGenerator.h
 *
 *  Created on: 20 feb. 2017
 */

#ifndef __QAPSOLGENERATOR_H__
#define __QAPSOLGENERATOR_H__

#ifndef __QAPINSTANCE_H__
#include "QAPInstance.h"
#else
class QAPInstance;
#endif

#ifndef __QAPSOLUTION_H__
#include "QAPSolution.h"
#else
class QAPSolution;
#endif

class QAPSolGenerator {
public:
	/*
	 * Obtiene una solución aleatoria.
	 */
	static void genRandomSol(QAPInstance &instance, QAPSolution &solution);
};

#endif /* __QAPSOLGENERATOR_H__ */
