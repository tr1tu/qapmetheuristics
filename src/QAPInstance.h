/*
 * QAPInstance.h
 *
 *  Created on: 20 feb. 2017
 */

#ifndef __QAPINSTANCE_H__
#define __QAPINSTANCE_H__

#include <vector>

#ifndef __QAPSOLUTION_H__
#include "QAPSolution.h"
#else
class QAPSolution;
#endif


class QAPInstance {
protected:
	/*  Definir variables miembro
	 * _size define el tamaño de la instancia. Es decir, el numero lugares y departamentos que existen.
	 * _flows define la matriz de flujos o pesos entre lugares.
	 * _distances define la matriz de distancias entre lugares.
	 */
	int _size;
	std::vector<std::vector<int> > _flows;
	std::vector<std::vector<int> > _distances;

public:
	QAPInstance();
	~QAPInstance();

	/*
	 * Función de lectura de una instancia desde un fichero.
	 * El formato de los ficheros es el siguiente:
	 * <tamaño de la instancia>
	 * <matriz de flujos (size*size)>
	 * <matriz de distancias (size*size)>
	 */
	void readInstance(char *filename);

	/*
	 * Observadores de las variables miembro.
	 */
	int getSize() const;

	int getFlow(int i, int j) const;

	int getDistance(int i, int j) const;

	/*
	 * Función que calcula el sumatorio de las distancias entre lugares
	 * multiplicadas por el flujo entre departamentos.
	 */
	long long getSumDistanceTimesFlow(QAPSolution &solution) const;

	/**
	 * Función que genera una permutación de los enteros de 0 a (size - 1)
	 * @param[in] size Tamaño de la permutación
	 * @param[out] perm Vector donde se almacenará la permutación
	 */
	static void randomPermutation(int size, std::vector<int> &perm);

	/*
	 * Función que recibe un objeto solución y los índices de dos
	 * departamentos y calcula la diferencia de la suma de distancias por flujos
	 * que se produce a lrealizar el cambio.
	 */
	int getDeltaSumDistanceTimesFlowSwap(QAPSolution &solution, int indexFacility1, int indexFacility2);

	/*
	 * Función que recibe un objeto solución, el índice del departamento y el índice de la localización
	 * y calcula la diferencia de la suma de distancias por flujos que se produce al realizar el cambio.
	 */
	int getDeltaSumDistanceTimesFlowAssignment(QAPSolution &solution, int indexFacility, int indexLocation);


};

#endif /* __QAPINSTANCE_H__ */
