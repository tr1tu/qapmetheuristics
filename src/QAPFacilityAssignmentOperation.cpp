/*
 * QAPFacilityAssignmentOperation.cpp
 *
 *  Created on: 9 mar. 2017
 */

#include "QAPFacilityAssignmentOperation.h"

QAPFacilityAssignmentOperation::QAPFacilityAssignmentOperation()
{
	_indexFacility = 0;
	_indexLocation = 0;
	_deltaFitness = 0;
}

QAPFacilityAssignmentOperation::~QAPFacilityAssignmentOperation(){ }

void QAPFacilityAssignmentOperation::apply(QAPSolution &solution)
{
	/*
	 * Aplicamos la oepración de asignación del departamento a la localización
	 * en la solución recibida.
	 * 1. Asignamos el departamento.
	 * 2. Actualizamos el fitness de la solución.
	 */

	//Asignamos el departamento a la localización indicada.
	solution.assignFacilityToLocation(_indexFacility, _indexLocation);

	//Actualizamos el fitness de la solución sumándole deltaFiness.
	solution.setFitness(solution.getFitness() + _deltaFitness);
}

void QAPFacilityAssignmentOperation::setValues(int indexFacility, int indexLocation, double deltaFitness)
{
	//Asignamos los valores de los atributos de clase.
	_indexFacility = indexFacility;
	_indexLocation = indexLocation;
	_deltaFitness = deltaFitness;
}

int QAPFacilityAssignmentOperation::getFacility() const
{
	return _indexFacility;
}

int QAPFacilityAssignmentOperation::getLocation() const
{
	return _indexLocation;
}
