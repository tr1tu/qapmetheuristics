/*
 * QAPInstance.cpp
 *
 *  Created on: 20 feb. 2017
 */

#include <fstream>
#include <cstdlib>
#include <ctime>
#include "QAPInstance.h"
#include <iostream>
#include <limits>

QAPInstance::QAPInstance() {
	/*
	 * Inicializar variables miembro.
	 */
	_size = 0;
}

QAPInstance::~QAPInstance() {}

int QAPInstance::getSize() const
{
	return _size;
}

int QAPInstance::getFlow(int i, int j) const
{
	return _flows.at(i).at(j);
}

int QAPInstance::getDistance(int i, int j) const
{
	return _distances.at(i).at(j);
}

void QAPInstance::readInstance(char *filename)
{
	/*
	 * Leer fichero de instancia.
	 */

	std::ifstream f(filename);

	if(f.is_open())
	{
		//Leemos el tamaño de la instancia.
		f >> _size;


		//Reservamos y leemos la matriz de distancias.
		_distances.resize(_size);
		for(int i = 0; i < _size; i++)
		{
			_distances[i].resize(_size);
			for(int j = 0; j < _size; j++)
			{
				f >> _distances[i][j];
			}
		}

		//Reservamos y leemos la matriz de flujos.
		_flows.resize(_size);
		for(int i = 0; i < _size; i++)
		{
			_flows[i].resize(_size);
			for(int j = 0; j < _size; j++)
			{
				f >> _flows[i][j];
			}
		}
	}


}

long long QAPInstance::getSumDistanceTimesFlow(QAPSolution &solution) const
{
	/* Calcular el sumatorio de las distancias multiplicadas por
	 * los flujos correspondientes, basandose en las asignaciones de departamentos
	 * a lugares de la solucion.
	 */

	long long sum = 0;

	for(int i = 0; i < _size; i++)
	{
		for(int j = 0; j < _size; j++)
		{
			//Si ninguna localizacion es valida, penaizamos sumando el coste como si estuviese
			//cada departamento en cada una de las posiciones.
			if(solution.whereIsFacility(i) < 0 && solution.whereIsFacility(j) < 0)
			{
				for(int k = 0; k < getSize(); k++)
					for(int t = 0; t < getSize(); t++)
						sum += _distances.at(k).at(t) * _flows.at(i).at(j);

			}
			//Si el departamento i no esta en posicion valida, penalizamos sumando como si el
			//departamento i estuviese en cada una de las posiciones.
			else if(solution.whereIsFacility(i) < 0)
			{
				for(int k = 0; k < getSize(); k++)
					sum += _distances.at(k).at(solution.whereIsFacility(j)) * _flows.at(i).at(j);

			}
			//Si el departamento j no esta en posicion valida, penalizamos sumando como si el
			//departamento j estuviese en cada una de las posiciones.
			else if(solution.whereIsFacility(j) < 0)
			{
				for(int k = 0; k < getSize(); k++)
					sum += _distances.at(solution.whereIsFacility(i)).at(k) * _flows.at(i).at(j);

			}
			else
			{
				sum += _distances.at(solution.whereIsFacility(i)).at(solution.whereIsFacility(j)) * _flows.at(i).at(j);
			}
		}
	}

	return sum;
}

void QAPInstance::randomPermutation(int size, std::vector<int> &perm)
{
	//Vaciamos el vector.
	perm.clear();

	//Rellenamos el vector con la permutación identidad.
	for(int i = 0; i < size; i++)
		perm.push_back(i);

	//Intercambiamos cada posición con otra aleatoria.
	for(int i = 0; i < size; i++)
	{
		int r = rand() % size;
		int aux = perm.at(i);
		perm.at(i) = perm.at(r);
		perm.at(r) = aux;
	}
}

int QAPInstance::getDeltaSumDistanceTimesFlowSwap(QAPSolution &solution, int indexFacility1, int indexFacility2)
{
	//Inicializamos el delta a 0.
	int deltaSumDistanceTimesFlow = 0;

	//Guardamos la antigua localización.
	int old_location1 = solution.whereIsFacility(indexFacility1);
	int old_location2 = solution.whereIsFacility(indexFacility2);

	/*int new_location1 = old_location2;
	int new_location2 = old_location1;

	for(int i=0; i<_size; i++)
	{
		if(i== solution.whichFacilityIsThere(old_location1) || i==solution.whichFacilityIsThere(old_location2))
		{
			continue;
		}

		//std::cout << i << std::endl;
		//Resto el valor del departamento i con la distancia a donde estaba el objeto1 y el flujo con el objeto 1 y viceversa
		deltaSumDistanceTimesFlow -= (_distances[solution.whereIsFacility(i)][old_location1] * _flows[i][indexFacility1]);
		deltaSumDistanceTimesFlow -= (_distances[old_location1][solution.whereIsFacility(i)] * _flows[indexFacility1][i]);

		//Sumo el valor de cada departamento con la distancia a la nueva localización por el flujo con el nuevo de partamento y viceversa
		deltaSumDistanceTimesFlow += (_distances[solution.whereIsFacility(i)][new_location2] * _flows[i][indexFacility2]);
		deltaSumDistanceTimesFlow += (_distances[new_location2][solution.whereIsFacility(i)] * _flows[indexFacility2][i]);

		//Resto el valor del departamento i con la distancia a donde estaba el objeto2 y el flujo con el objeto 2 y viceversa
		deltaSumDistanceTimesFlow -= (_distances[solution.whereIsFacility(i)][old_location2] * _flows[i][indexFacility2]);
		deltaSumDistanceTimesFlow -= (_distances[old_location2][solution.whereIsFacility(i)] * _flows[indexFacility2][i]);


		//Sumo el valor del departamento i con la distancia a donde estaba el objeto1 y el flujo con el objeto 1 y viceversa
		deltaSumDistanceTimesFlow += (_distances[solution.whereIsFacility(i)][new_location1] * _flows[i][indexFacility1]);
		deltaSumDistanceTimesFlow += (_distances[new_location1][solution.whereIsFacility(i)] * _flows[indexFacility1][i]);

	}

	deltaSumDistanceTimesFlow -= (_distances[old_location1][old_location2] * _flows[indexFacility1][indexFacility2]);
	deltaSumDistanceTimesFlow -= (_distances[old_location2][old_location1] * _flows[indexFacility2][indexFacility1]);

	deltaSumDistanceTimesFlow +=(_distances[new_location1][new_location2] * _flows[indexFacility1][indexFacility2]);
	deltaSumDistanceTimesFlow +=(_distances[new_location2][new_location1] * _flows[indexFacility2][indexFacility1]);*/

	//std::cout << "DeltaTimeFlow << " << deltaSumDistanceTimesFlow << std::endl;


	long long currentSumDistanceTimesFlow = getSumDistanceTimesFlow(solution);

	solution.assignFacilityToLocation(indexFacility1, old_location2);
	solution.assignFacilityToLocation(indexFacility2, old_location1);

	deltaSumDistanceTimesFlow = getSumDistanceTimesFlow(solution) - currentSumDistanceTimesFlow;

	solution.assignFacilityToLocation(indexFacility1, old_location1);
	solution.assignFacilityToLocation(indexFacility2, old_location2);

	//std::cout << "CORRECTO" << deltaSumDistanceTimesFlow << std::endl;



/*	for(int i = 0; i < _size; i++)
	{
		//Restamos el valor de (facility1,location1) <-> (i, location i)
		deltaSumDistanceTimesFlow -= _distances[location1][solution.whereIsFacility(i)] * _flows[indexFacility1][i];
		//Restamos el valor de (facility2,location2) <-> (i, location i)
		deltaSumDistanceTimesFlow -= _distances[location2][solution.whereIsFacility(i)] * _flows[indexFacility2][i];
		//Sumamos el valor de (facility1,location2) <-> (i, location i) ya que facility1 ahora está en location2.
		deltaSumDistanceTimesFlow += _distances[location2][solution.whereIsFacility(i)] * _flows[indexFacility1][i];
		//Sumamos el valor de (facility2,location1) <-> (i, location i) ya que facility2 ahora está en location1.
		deltaSumDistanceTimesFlow += _distances[location1][solution.whereIsFacility(i)] * _flows[indexFacility2][i];
	}*/



	return deltaSumDistanceTimesFlow;
}


int QAPInstance::getDeltaSumDistanceTimesFlowAssignment(QAPSolution &solution,
		int indexFacility, int indexLocation)
{
	int deltaSumDistanceTimesFlow = 0;

	int location1 = solution.whereIsFacility(indexFacility);
	int facility2 = solution.whichFacilityIsThere(indexLocation);

	long long currentSumDistanceTimesFlow = getSumDistanceTimesFlow(solution);

	solution.assignFacilityToLocation(indexFacility, indexLocation);
	if(facility2 >= 0)
		solution.assignFacilityToLocation(facility2, location1);

	deltaSumDistanceTimesFlow = getSumDistanceTimesFlow(solution) - currentSumDistanceTimesFlow;

	solution.assignFacilityToLocation(indexFacility, location1);

	if(facility2 >= 0)
		solution.assignFacilityToLocation(facility2, indexLocation);

	return deltaSumDistanceTimesFlow;
}



/*for(int i = 0; i < _size; i++)
	{
		if(i == indexFacility1 || i == indexFacility2)
			continue;

		//Restamos el valor de (facility1,location1) -> (i, location i)
		deltaSumDistanceTimesFlow -= _distances[location1][solution.whereIsFacility(i)] * _flows[indexFacility1][i];
		//Restamos el valor de (i, location i) -> (facility1,location1)
		deltaSumDistanceTimesFlow -= _distances[solution.whereIsFacility(i)][location1] * _flows[i][indexFacility1];
		//Restamos el valor de (facility2,location2) -> (i, location i)
		deltaSumDistanceTimesFlow -= _distances[location2][solution.whereIsFacility(i)] * _flows[indexFacility2][i];
		//Restamos el valor de (i, location i) -> (facility2,location2)
		deltaSumDistanceTimesFlow -= _distances[solution.whereIsFacility(i)][location2] * _flows[i][indexFacility2];
		//Sumamos el valor de (facility1,location2) -> (i, location i) ya que facility1 ahora está en location2.
		deltaSumDistanceTimesFlow += _distances[location2][solution.whereIsFacility(i)] * _flows[indexFacility1][i];
		//Sumamos el valor de (i, location i) -> (facility1,location2) ya que facility1 ahora está en location2.
		deltaSumDistanceTimesFlow += _distances[solution.whereIsFacility(i)][location2] * _flows[i][indexFacility1];
		//Sumamos el valor de (facility2,location1) -> (i, location i) ya que facility2 ahora está en location1.
		deltaSumDistanceTimesFlow += _distances[location1][solution.whereIsFacility(i)] * _flows[indexFacility2][i];
		//Sumamos el valor de (i, location i) -> (facility2,location1) ya que facility2 ahora está en location1.
		deltaSumDistanceTimesFlow += _distances[solution.whereIsFacility(i)][location1] * _flows[i][indexFacility2];
	}*/

