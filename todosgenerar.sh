#!/bin/bash

if [ ! -d resultados ]; then
	mkdir resultados
fi

for file in $(find $1)
do
	
	if [ -f $file ]; then

	instance="$(basename "$file")"
	instance="${instance%.*}"
	resultsfile="resultados/$instance"_results.txt
	./Debug/qapmetheuristics.exe $file > "$resultsfile"

	output="resultados/$instance".png

cat << _end_ | gnuplot
	set terminal png
	set output "$output"
	set key right top
	set xlabel "Numero de ejecuciones"
	set ylabel "Fitness"
	plot '$resultsfile' using 1 with linespoints pi 10000 title 'Current First', '$resultsfile' using 2 with linespoints pi 10000 title 'Best First', '$resultsfile' using 3 with linespoints pi 10000 title 'Current Best', '$resultsfile' using 4 with linespoints pi 10000 title 'Best Best'
_end_
	
	echo -e "Generado: $output"

	fi

done

