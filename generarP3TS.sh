#!/bin/bash

if [ ! -d resultados ]; then
	mkdir resultados
fi

for file in "$@"
do
	
	instance="$(basename "$file")"
	instance="${instance%.*}"
	resultsfile="resultados/$instance"_P3TS_results.txt
	
	if [ -e ./Debug/qapmetheuristics.exe ]; then
		./Debug/qapmetheuristics.exe $file > "$resultsfile"
	fi
	if [ -e ./Debug/qapmetheuristics ]; then
		./Debug/qapmetheuristics $file > "$resultsfile"
	fi
	if [ -e ./Debug/QAPMetheuristics ]; then
		./Debug/QAPMetheuristics $file > "$resultsfile"
	fi

	output="resultados/P3TS_$instance".png

cat << _end_ | gnuplot
	set terminal png
	set output "$output"
	set title "$instance - Taboo Search"
	set key right top
	set xlabel "Numero de ejecuciones"
	set ylabel "Fitness"
	set logscale x
	plot '$resultsfile' using 3 with lines title 'Current TS', '$resultsfile' using 4 with lines title 'Best TS'
_end_
	
	echo -e "Generado: $output"



done

