#!/bin/bash

if [ ! -d resultados ]; then
	mkdir resultados
fi

for file in "$@"
do
	
	instance="$(basename "$file")"
	instance="${instance%.*}"
	resultsfile="resultados/$instance"_P4ACO_results.txt
	
	if [ -e ./Debug/qapmetheuristics.exe ]; then
		./Debug/qapmetheuristics.exe $file > "$resultsfile"
	fi
	if [ -e ./Debug/qapmetheuristics ]; then
		./Debug/qapmetheuristics $file > "$resultsfile"
	fi
	if [ -e ./Debug/QAPMetheuristics ]; then
		./Debug/QAPMetheuristics $file > "$resultsfile"
	fi

	output="resultados/P4ACO_$instance".png

cat << _end_ | gnuplot
	set terminal png
	set output "$output"
	set title "$instance - ACO"
	set key right top
	set xlabel "Numero de ejecuciones"
	set ylabel "Fitness"
	set logscale x
	plot '$resultsfile' using 3 with points ps 0.2 title 'Current ACO', '$resultsfile' using 4 with lines title 'Best ACO'
_end_
	
	echo -e "Generado: $output"



done

