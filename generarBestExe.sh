#!/bin/bash

if [ ! -d resultados ]; then
	mkdir resultados
fi

for file in "$@"
do
	
	instance="$(basename "$file")"
	instance="${instance%.*}"
	resultsfile="resultados/$instance"_results.txt
	./Debug/qapmetheuristics.exe $file > "$resultsfile"

	output="resultados/$instance".png

cat << _end_ | gnuplot
	set terminal png
	set output "$output"
	set title "$instance"
	set key right top
	set xlabel "Numero de ejecuciones"
	set ylabel "Fitness"
	set logscale x
	plot '$resultsfile' using 2 with lines title 'Best SA', '$resultsfile' using 4 with lines title 'Best TS', '$resultsfile' using 6 with lines title 'Best GRASP', '$resultsfile' using 8 with lines title 'Best IG'
_end_
	
	echo -e "Generado: $output"



done

