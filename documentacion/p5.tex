\documentclass[12pt, a4paper, oneside, titlepage]{report}
\usepackage[utf8]{inputenc}
\usepackage[spanish, es-tabla]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{cite}
\usepackage{url}
\usepackage[hidelinks]{hyperref}
\usepackage{longtable}
\usepackage{float}
\author{Álvaro Cardador Pedraza, Alejandro Almagro Santos, Emilio Aranda Caballero, Víctor Manuel Vargas Yun}
\title{Metaheurísticas. Práctica 5}
\date{Curso académico 2016-2017\\Córdoba, \today}

\newcommand{\myparagraph}[1]{\paragraph{#1}\mbox{}\\}
\newcommand{\mysubparagraph}[1]{\subparagraph{#1}\mbox{}\\}

\usepackage{color}

\definecolor{pblue}{rgb}{0.13,0.13,1}
\definecolor{pgreen}{rgb}{0,0.5,0}
\definecolor{pred}{rgb}{0.9,0,0}
\definecolor{pgrey}{rgb}{0.46,0.45,0.48}

\usepackage{listings}
\lstset{language=C,
  showspaces=false,
  showtabs=false,
  breaklines=true,
  showstringspaces=false,
  breakatwhitespace=true,
  commentstyle=\color{pgreen},
  keywordstyle=\color{pblue},
  stringstyle=\color{pred},
  basicstyle=\ttfamily\scriptsize,
  inputencoding=latin9
  %moredelim=[il][\textcolor{pgrey}]{$$},
  %moredelim=[is][\textcolor{pgrey}]{\%\%}{\%\%}
}

\begin{document}

\begin{titlepage}
\centering
\includegraphics[width=0.20\textwidth]{uco.png}
\includegraphics[width=0.18\textwidth]{eps.jpg}\par\vspace{1cm}
{\scshape\LARGE Universidad de Córdoba\par}
{\scshape\Large Escuela Politécnica Superior de Córdoba\par}
\vspace{1cm}
{\scshape\LARGE Ingeniería Informática\par}
{\scshape\Large Especialidad: Computación\par}
{\scshape\Large Tercer curso. Segundo cuatrimestre\par}
\vspace{1cm}
{\scshape\LARGE Metaheurísticas.\par}
{\scshape\Large Práctica 5.\par}
\vspace{0.5cm}
{\huge\bfseries Comparación de metaheurísticas\par}
\vspace{1.2cm}
{\Large\itshape Álvaro Cardador Pedraza\par}
{\Large\itshape Alejandro Almagro Santos\par}
{\Large\itshape Emilio Aranda Caballero\par}
{\Large\itshape Víctor Manuel Vargas Yun\par}
\vfill

{\large Curso académico 2016-2017\\Córdoba, \today\par}
\end{titlepage}

\tableofcontents

\setlength{\parskip}{9pt} %distancia entre párrafos

\chapter{Aspectos generales del QAP}
\section{Descripción del problema}
El \emph{Quadratic Assignment Problem} (\textbf{QAP}) es un problema de complejidad NP en el que tenemos N localizaciones, y N departamentos o instalaciones que tenemos que asignar a cada una de las localizaciones. Cada par de departamentos tiene asignado un flujo que indica el movimiento relativo que se produce de personas, mercancías,... entre dos departamentos. Cada par de localizaciones tiene asignada una distancia.

El problema consiste en encontrar la asignación de departamentos a localizaciones que minimice el sumatorio de productos de flujo por distancia, teniendo en cuenta que todos los departamentos deben estar asignados a una localización y en cada localización sólamente puede haber un departamento. Como se puede apreciar, se trata de un problema de minimización.

\section{Representación de las soluciones}
Las soluciones se representan mediante un vector, donde el índice indica el departamento y el valor representa la localización que tiene asignada. El número de localizaciones y departamentos coincide por lo tanto aparecerá siempre cada uno sin repeticiones. Se trata de una codificación de orden, por lo que las permutaciones serán un buen método de variación de la solución. En la Figura \ref{fig:VectorSoluciones} se muestra este vector.

\begin{figure}[H]
\centering
\includegraphics[scale=0.6]{vector_soluciones.png}
\caption{Representación del vector solución}
\label{fig:VectorSoluciones}
\end{figure}

\section{Función de evaluación}
El fitness de la solución será calculado como el sumatorio de productos de flujo por distancia para cada uno de los departamentos.

\begin{center}
$fitness = \sum_{i=0}^n \sum_{j=0}^n dist(L(i),L(j))flujo(i,j)$
\end{center}

El valor de fitness se intentará minimizar.

\chapter{Metaheurísticas consideradas}
A continuación se detallarán las diferentes metaheurísticas que se han considerado para la resolución del QAP. Para cada una de ellas, se han realizado experimentos con 4 instancias: \emph{chr12a}, \emph{chr18b}, \emph{bur26a} y \emph{lipa50a}. Las dos primeras, son instancias relativamente pequeñas. Las dos últimas son instancias mayores.

El objetivo de la experimentación es estudiar qué metaheurísticas se ajustan mejor al problema elegido y una vez identificadas, intentar ajustar sus parámetros para conseguir los mejores resultados posibles.
\section{Búsqueda aleatoria}
\subsection{Adaptaciones para el problema}
En el QAP, es necesario comprobar que en las soluciones aleatorias generadas no haya localizaciones que se repitan en varios departamentos ni departamentos que no tengan una localización válida.
Para ello, se rellena el vector solución con una permutación aleatoria de los números comprendidos entre 0 y el tamaño de la instancia menos uno.

\subsection{Experimentación}
\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P1_chr12a.png}
\caption{Búsqueda aleatoria en chr12a}
\label{fig:RSchr12a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P1_chr18b.png}
\caption{Búsqueda aleatoria en chr18b}
\label{fig:RSchr18b}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P1_bur26a.png}
\caption{Búsqueda aleatoria en bur26a}
\label{fig:RSbur26a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P1_lipa50a.png}
\caption{Búsqueda aleatoria en lipa50a}
\label{fig:RSlipa50a}
\end{figure}

Observando las Figuras \ref{fig:RSchr12a}, \ref{fig:RSchr18b}, \ref{fig:RSbur26a} y \ref{fig:RSlipa50a}, se puede ver que los valores de current son totalmente aleatorios: lo mismo puede tomar un valor muy alto y un valor muy bajo en el siguiente punto. El valor de best se puede observar que va siempre reduciéndose, ya que es un problema de minimización. Las observaciones realizadas dejan ver que la búsqueda aleatoria ha sido implementada correctamente.

\section{Búsqueda local}
\subsection{Adaptaciones para el problema}
En el caso de la búsqueda local, hemos decidido que la operación que se utilizará para explorar el vecindario será la de permutación. Esta es una forma fácil y eficiente de explorar el vecindario en los problemas que utilizan una codificación de la solución de orden ya que no caemos en soluciones que no sean válidas.

\subsection{Experimentación}
A continuación se muestran los resultados de la búsqueda local con las instancias seleccionadas. Se está utilizando por un lado la búsqueda local con primera mejora y por otro lado la búsqueda local con mejor mejora.
\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P2_chr12a.png}
\caption{Búsqueda local en chr12a}
\label{fig:LSchr12a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P2_chr18b.png}
\caption{Búsqueda local en chr18b}
\label{fig:LSchr18b}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P2_bur26a.png}
\caption{Búsqueda local en bur26a}
\label{fig:LSbur26a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P2_lipa50a.png}
\caption{Búsqueda local en lipa50a}
\label{fig:LSlipa50a}
\end{figure}

Los experimentos realizados se pueden observar en las Figuras \ref{fig:LSchr12a}, \ref{fig:LSchr18b}, \ref{fig:LSbur26a} y \ref{fig:LSlipa50a}. Dichos experimentos consisten en tomar 5 soluciones aleatorias iniciales y a partir de ellas empezar una búsqueda local. Esto se puede ver reflejado en la gráfica en cada uno de los picos. En el caso de las instancias más grandes, la búsqueda local con mejor mejora no ha tenido tiempo suficiente de optimizar las 5 soluciones, ya que esta tiene que explorar todo el vecindario para elegir el mejor vecino, lo que hace que sea más costosa computacionalmente.

Para comprobar que la búsqueda local se ha implementado correctamente, basta con ver que la gráfica tiene un comportamiento monótono decreciente en cada una de las 5 soluciones que han sido optimizadas.

\section{Enfriamiento simulado}
\subsection{Adaptaciones para el problema}
En el enfriamiento simulado para QAP, de nuevo utilizamos las permutaciones para realizar cambios en las soluciones. Se realiza una elección de dos departamentos aleatorios y se intercambian sus localizaciones.

En la función de aceptación, al ser un problema de minimización, se debe invertir el signo del deltaFitness antes de utilizarlo para calcular la probabilidad de aceptación.

\subsection{Experimentación}
Para nuestra experimentación, hemos configurado el enfriamiento simulado para que utilice los siguientes parámetros:
\begin{itemize}
\item Probabilidad de aceptación inicial: $0.9$
\item Constante de enfriamiento: $0.997$
\item Iteraciones para enfriar: $50$
\end{itemize}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P3SA_chr12a.png}
\caption{Enfriamiento simulado en chr12a}
\label{fig:SAchr12a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P3SA_chr18b.png}
\caption{Enfriamiento simulado en chr18b}
\label{fig:SAchr18b}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P3SA_bur26a.png}
\caption{Enfriamiento simulado en bur26a}
\label{fig:SAbur26a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P3SA_lipa50a.png}
\caption{Enfriamiento simulado en lipa50a}
\label{fig:SAlipa50a}
\end{figure}

Si observamos las Figuras \ref{fig:SAchr12a}, \ref{fig:SAchr18b}, \ref{fig:SAbur26a} y \ref{fig:SAlipa50a}, podemos apreciar como el valor de current va tomando valores aleatorios. Sin embargo, conforme avanzan las iteraciones, las soluciones se van tomando en un rango más pequeño. Esto se debe a que la temperatura se va enfriando y se reduce la probabilidad de aceptar una solución que sea peor. Sin embargo, las soluciones a mejor siempre se van aceptando. Este comportamiento es un indicador de que la metaheurística ha sido implementada de forma correcta.

\subsection{Optimización de parámetros}
Estas instancias de enfriamiento simulado se han lanzado con una optimización previa de los parámetros.

\begin{itemize}
\item La probabilidad inicial de aceptar una solución a peor se ha fijado en 0.95, de manera que al aumentar la probabilidad un 0.05\% iremos aceptando soluciones a peor con más probabilidad, por lo tanto al principio será más diversificador.
\item También hemos disminuido el factor de enfriamiento, de manera que el sistema se enfría más rápido, aceptando así soluciones a peor con menos probabilidad a medida que avanza el algoritmo.
\item Por último, hemos puesto el número de iteraciones para enfriar en 20, para intensificar más aún conforme avanza el algoritmo.
\end{itemize}

La conclusión es que llegamos a soluciones muy similares en muchas menos iteraciones.

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{enfriamiento/P3SA_chr12a.png}
\caption{Enfriamiento simulado optimizado en chr12a}
\label{fig:SAOpchr12a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{enfriamiento/P3SA_chr18b.png}
\caption{Enfriamiento simulado optimizado en chr18b}
\label{fig:SAOpchr18b}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{enfriamiento/P3SA_bur26a.png}
\caption{Enfriamiento simulado optimizado en bur26a}
\label{fig:SAOpbur26a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{enfriamiento/P3SA_lipa50a.png}
\caption{Enfriamiento simulado optimizado en lipa50a}
\label{fig:SAOplipa50a}
\end{figure}

\section{Búsqueda tabú}
\subsection{Adaptaciones para el problema}
En nuestro problema, hemos considerado que cuando realizamos una acción de intercambiar departamentos, almacenamos en la memoria tabú el cambio del departamento 1 a la posición del departamento 2 como el departamento 2 a la posición del departamento 1. Como las inserciones en la memoria tabú se realizan por pares, cuando la memoria está llena y se tienen que
añadir más elementos, se realizan borrados por pares también.

\subsection{Experimentación}
Este primer experimento se ha realizado utilizando una tenencia tabú de $\frac{size}{2.5}$.
\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P3TS_chr12a.png}
\caption{Búsqueda tabú en chr12a}
\label{fig:TSchr12a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P3TS_chr18b.png}
\caption{Búsqueda tabú en chr18b}
\label{fig:TSchr18b}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P3TS_bur26a.png}
\caption{Búsqueda tabú en bur26a}
\label{fig:TSbur26a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P3TS_lipa50a.png}
\caption{Búsqueda tabú en lipa50a}
\label{fig:TSlipa50a}
\end{figure}

Se puede ver que la metaheurística funciona bastante bien, pues consigue llegar a una solución bastante buena en muy pocas iteraciones. Posteriormente hay algunas subidas y bajadas en el current que probablemente se producen porque está intentando buscar una solución mejor pero ya no es posible encontrarla. Sin embargo, aunque la solución va empeorando, no puede volver a la solución óptima hasta que no salen de la memoria tabú los movimientos que le permiten volver a esta.

\subsection{Optimización de parámetros}
Con la intención de conseguir mejores resultados, se ha modificado la tenencia tabú. En este caso, los experimentos se han realizado con una tenencia tabú de $size$.
\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{tenencia1/P3TS_chr12a.png}
\caption{Búsqueda tabú optimizada en chr12a}
\label{fig:TSOpchr12a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{tenencia1/P3TS_chr18b.png}
\caption{Búsqueda tabú optimizada en chr18b}
\label{fig:TSOpchr18b}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{tenencia1/P3TS_bur26a.png}
\caption{Búsqueda tabú optimizada en bur26a}
\label{fig:TSOpbur26a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{tenencia1/P3TS_lipa50a.png}
\caption{Búsqueda tabú optimizada en lipa50a}
\label{fig:TSOplipa50a}
\end{figure}

Como se puede observar, los cambios en el fitness de la mejor solución encontrada son prácticamente inexistentes. Por tanto, podemos ver que la metaheurística funciona igual independientemente de la tenencia tabú que se utilice.

\section{GRASP}
\subsection{Adaptaciones para el problema}
En el caso del GRASP, cuando se realiza la construcción de la solución inicial mediante el método greedy aleatorizado, se utilizan también operaciones de intercambio para buscar la mejor solución.
Para la fase de búsqueda local, como ya se ha comentado antes, se utilizan permutaciones para explorar el vecindario. Se ha utilizado la búsqueda local con primera mejora ya que el rendimiento de ambos tipos era muy similar y la primera mejora es más rápida que la mejor mejora.

\subsection{Experimentación}
Para la experimentación de GRASP, se ha reducido el número de soluciones generadas con el objetivo de poder apreciar correctamete el comportamiento del mismo en las gráficas.
\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P3GRASP_chr12a.png}
\caption{GRASP en chr12a}
\label{fig:GRASPchr12a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P3GRASP_chr18b.png}
\caption{GRASP en chr18b}
\label{fig:GRASPchr18b}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P3GRASP_bur26a.png}
\caption{GRASP en bur26a}
\label{fig:GRASPbur26a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P3GRASP_lipa50a.png}
\caption{GRASP en lipa50a}
\label{fig:GRASPlipa50a}
\end{figure}

En las Figuras \ref{fig:GRASPchr12a}, \ref{fig:GRASPchr18b}, \ref{fig:GRASPbur26a} y \ref{fig:GRASPlipa50a}, se puede comprobar que el funcionamiento de GRASP es correcto ya que está generando soluciones greedy aleatorizadas que posteriormente son optimizadas mediante búsqueda local. Las soluciones greedy se corresponden con los picos de la gráfica y la optimización local con la pendiente hacia abajo.

En donde mejor se puede observar la gráfica es en la instancia lipa50a, que debido a su tamaño, tiene tiempo de generar menos soluciones.

\section{Iterated Greedy}
\subsection{Adaptaciones para el problema}
En el Iterated Greedy para QAP, la destrucción de la solución se realiza mediante la asignación de la localización -1 al departamento que se quiere destruir. Para reconstruir las soluciones, se buscan los departamentos que estén asignados a la localización -1 y se busca la mejor localización, que esté libre, en la que se puede asignar.

\subsection{Experimentación}
En este caso también se ha reducido el número de soluciones generadas para permitir apreciar correctamente el comportamiento de las gráficas. Además, se ha utilizado escala logarítmica en el eje Y ya que cuando se destruye una solución, los valores de fitness resultantes son muy altos debido a las penalizaciones que aplicamos a una solución no válida. Si no se aplicara la escala logarítmica, estos valores impedirían ver el comportamiento del resto de valores.
\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P3IG_chr12a.png}
\caption{Iterated Greedy en chr12a}
\label{fig:IGchr12a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P3IG_chr18b.png}
\caption{Iterated Greedy en chr18b}
\label{fig:IGchr18b}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P3IG_bur26a.png}
\caption{Iterated Greedy en bur26a}
\label{fig:IGbur26a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P3IG_lipa50a.png}
\caption{Iterated Greedy en lipa50a}
\label{fig:IGlipa50a}
\end{figure}

Como se puede observar en las Figuras \ref{fig:IGchr12a}, \ref{fig:IGchr18b}, \ref{fig:IGbur26a} y \ref{fig:IGlipa50a}, partimos de una solución aleatoria que se empieza a destruir, lo que genera unos valores de fitness muy altos que se corresponden con los picos de la gráfica, y posteriormente se realiza una reconstrucción de la misma, que se refleja en la gráfica en las pendientes descendentes que siguen a cada uno de estos picos.

Esto se corresponde con el comportamiento normal que debe tener un Iterated Greedy. Por lo tanto, podemos pensar que la metaheurística está implementada correctamente. 

\section{Algoritmo genético}
\subsection{Adaptaciones para el problema}
En el algoritmo genético para QAP, hay que implementar operadores de cruce y de mutación para codificación de soluciones basadas en orden.

El operador de mutación consiste en recorrer todos los departamentos e intermcabiarlo con otro departamento aleatorio con una determinada probabilidad.

El operador de cruce, es algo más complejo ya que hay que evitar que se repitan las localizaciones o que algún departamento se quede sin localización. El funcionamiento del operador implementado consiste en lo siguiente:
\begin{enumerate}
\item Seleccionar dos puntos de corte en los vectores solución.
\item Copiar las localizaciones de los departamentos comprendidos entre los dos puntos de corte desde el padre 1 hasta el hijo. Marcar estas localizaciones en el padre 2 para evitar que se repitan.
\item Insertar las localizaciones restantes del padre 2 en el hijo por el lado derecho hasta completar todos los departamentos.
\end{enumerate}

En la Figura \ref{fig:OperadorCruce} se puede observar de forma gráfica el funcionamiento del operador de cruce.

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{operador_cruce.png}
\caption{Operador de cruce para el algoritmo genético}
\label{fig:OperadorCruce}
\end{figure}

Con este operador, se puede garantizar que todos los departamentos tengan una localización asignada sin repetir ninguna localización.

\subsection{Experimentación}
Para mejorar la visualización de la gráfica, se ha utilizado escala logarítmica en el eje X.
\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P4GA_chr12a.png}
\caption{Algoritmo genético en chr12a}
\label{fig:GAchr12a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P4GA_chr18b.png}
\caption{Algoritmo genético en chr18b}
\label{fig:GAchr18b}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P4GA_bur26a.png}
\caption{Algoritmo genético en bur26a}
\label{fig:GAbur26a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P4GA_lipa50a.png}
\caption{Algoritmo genético en lipa50a}
\label{fig:GAlipa50a}
\end{figure}

Como se puede observar en la Figuras \ref{fig:GAchr12a}, \ref{fig:GAchr18b}, \ref{fig:GAbur26a} y \ref{fig:GAlipa50a}, el algoritmo genético está proporcionando una solución final con fitness aceptable. Sin embargo, el algoritmo genético en el QAP tiene un comportamiento bastante aleatorio y no se puede observar que tienda claramente a mejorar la solución. Simplemente va creando nuevas poblaciones a partir de los mejores de individuos pero en muchos casos, el cruce que producen dos individuos bastante buenos resulta ser una solución bastante mala.

\section{Algoritmo genético con búsqueda local}
Debido a que el algoritmo genético por sí solo no ha conseguido resultados demasiado buenos, se le ha añadido una búsqueda local que tratará de optimizar la mejor solución que encuentre el algoritmo genético.
\subsection{Adaptaciones para el problema}
Las adaptaciones realizadas para esta metaheurística ya han sido explicadas en el algoritmo genético y en la búsqueda local.

\subsection{Experimentación}
\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P4GALS_chr12a.png}
\caption{Algoritmo genético con búsqueda local en chr12a}
\label{fig:GALSchr12a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P4GALS_chr18b.png}
\caption{Algoritmo genético con búsqueda local en chr18b}
\label{fig:GALSchr18b}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P4GALS_bur26a.png}
\caption{Algoritmo genético con búsqueda local en bur26a}
\label{fig:GALSbur26a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P4GALS_lipa50a.png}
\caption{Algoritmo genético con búsqueda local en lipa50a}
\label{fig:GALSlipa50a}
\end{figure}

En este caso, se ha ejecutado el algoritmo genético igual que en el caso anterior pero una vez que se ha terminado de ejecutar, se selecciona la mejor solución encontrada hasta el momento y se le realiza una optimización local. Con el objetivo de que la búsqueda local se aprecie mejor, no se ha aplicado escala logarítmica.

El comportamiento de la gráfica es similar al de antes pero se puede apreciar como al final se realiza la optimización local, generando varias soluciones mejores.

\section{Optimización de colonias de hormigas}
\subsection{Adaptaciones para el problema}
En el algoritmo de optimización de colonias de hormigas para ACO, la forma de buscar la alternativas que utilizan las hormigas para moverse es mediante permutaciones. Se utiliza una lista que contiene los departamentos que todavía no se han permutado. A la hora de generar las alternativas, se recorren los departamentos que estén en esta lista. Cuando se elige una operación, se eliminan los dos departamentos que se hayan intercambiado de la lista de departamentos restantes.

La matriz de feromonas representa en las filas el número de departamento y en las columnas la localización, de manera que indica la feromona que hay en el movimiento de asignar un departamento a una determianda localización. Para calcular la relevancia de una alternativa, se hace de la siguiente forma:

\begin{center}
$relevance = density^\beta \times ph(f_1,l_2)^\alpha \times ph(f_2,l_1)^\alpha$
\end{center}

siendo:
\begin{description}
\item[$f_1$] departamento 1.
\item[$f_2$] departamento 2.
\item[$l_1$] localización 1.
\item[$l_2$] localización 2.
\end{description}

En la fórmula intervienen dos índices de departamentos y dos de localizaciones porque como se ha comentado antes, las operaciones son de intercambio.

\subsection{Experimentación}
Las gráficas se han vuelto a representar en escala logarítmica en el eje X ya que el algoritmo de hormigas es bastante lento y en caso contrario no se apreciaría correctamente el comportamiento.
\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P4ACO_chr12a.png}
\caption{Optimización de colonias de hormigas en chr12a}
\label{fig:ACOchr12a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P4ACO_chr18b.png}
\caption{Optimización de colonias de hormigas en chr18b}
\label{fig:ACOchr18b}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P4ACO_bur26a.png}
\caption{Optimización de colonias de hormigas en bur26a}
\label{fig:ACObur26a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P4ACO_lipa50a.png}
\caption{Optimización de colonias de hormigas en lipa50a}
\label{fig:ACOlipa50a}
\end{figure}

Los resultados se corresponden con un algoritmo basado en poblaciones.
El problema del algoritmo de hormigas es que necesita muchas iteraciones, sobre todo en instancias grandes, para que las hormigas vayan explorando el espacio de búsqueda completo, tener información del problema y tomar decisiones correctas en función de la matriz de feromonas.
Otro inconveniente, es que una iteración que genere una solución nueva al problema consume mucho tiempo, por lo que termina antes de aprovechar la información de la matriz de feromonas y no genera soluciones tan buenas como los algoritmos basados en trayectorias, que generan nuevas soluciones muy rápidamente.

\section{Optimización de colonias de hormigas con búsqueda local}
\subsection{Adaptaciones para el problema}
Las adaptaciones realizadas son las mismas que para el algoritmo de hormigas sin búsqueda local.
La diferencia de esta metaheurística está en que después de obtener una solución con ACO, se realiza
una optimización local.

\subsection{Experimentación}
Las gráficas se han vuelto a representar en escala logarítmica en el eje X ya que el algoritmo de hormigas es bastante lento y en caso contrario no se apreciaría correctamente el comportamiento.
\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P4ACOLS_chr12a.png}
\caption{Optimización de colonias de hormigas con LS en chr12a}
\label{fig:ACOLSchr12a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P4ACOLS_chr18b.png}
\caption{Optimización de colonias de hormigas con LS en chr18b}
\label{fig:ACOLSchr18b}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P4ACOLS_bur26a.png}
\caption{Optimización de colonias de hormigas con LS en bur26a}
\label{fig:ACOLSbur26a}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{P4ACOLS_lipa50a.png}
\caption{Optimización de colonias de hormigas con LS en lipa50a}
\label{fig:ACOLSlipa50a}
\end{figure}

En las Figuras \ref{fig:ACOLSchr12a}, \ref{fig:ACOLSchr18b}, \ref{fig:ACOLSbur26a} y \ref{fig:ACOLSlipa50a} se puede observar cómo la búsqueda local al final de la ejecución del algoritmo de optimización de colonias de hormigas, aporta una mejora interesante al fitness de la solución. Esta mejora, es especialmente importante en las instancias más grandes, como es el caso de \emph{lipa50a}.
Como el algoritmo de hormigas no termina de explotar el conocimiento de la matriz de feromonas, se queda lejos de un óptimo, por lo que aplicar una búsqueda local a una solución que está medianamente orientada se mejora bastante el fitness de la solución.
\chapter{Comparativa experimental}
\section{Mejores soluciones}
Con el objetivo de poder comparar la calidad de las diferentes MHs con las que hemos experimentado, se ha hecho una comparación de las mejores soluciones que ha obtenido cada una de ellas y se han representado en la gráfica de la Figura \ref{fig:ComparacionBest}. Para ello, se han ejecutado cada una de las metaheurísticas con un límite de 500.000 evaluaciones y 30 segundos.
\begin{figure}[ht]
\centering
\includegraphics[scale=0.65]{comparacion_best.png}
\caption{Comparativa de MHs según la mejor solución obtenida}
\label{fig:ComparacionBest}
\end{figure}

El valor que se muestra en el eje Y se corresponde con la media de fitness que ha obtenido cada una de las metaheurísticas en las cuatro instancias que se han usado.

Como era de esperar, la búsqueda aleatoria ha obtenido el peor de los resultados. En segundo lugar se encuentra el algoritmo genético, que como ya se ha comentado antes,
su comportamiento es bastante aleatorio. Sin embargo, el algoritmo genético con búsqueda local mejora bastante. En el primer lugar tenemos el GRASP.

Es interesante ver como en el caso de la optimización de colonias de hormigas, al aplicarle al final la búsqueda local, se convierte en la segunda mejor metaheurística mientras que si no le aplicamos la búsqueda local, el resultado obtenido es bastante malo comparado con el de las demás.

\chapter{Grado de implicación de los miembros del grupo}
Los integrantes del grupo hemos participado al máximo en la elaboración de las prácticas, así como comentarlo y elaborar gráficas, documentos etc..
Cabe destacar la participación de Víctor Vargas, como líder del grupo que ha organizado el trabajo y que ha generado scripts para ejecutar algoritmos y generar las gráficas para avanzar más rápido a la hora de redactar las prácticas.

\end{document}